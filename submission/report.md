---
geometry:
- margin=1in
...


\author{Reinis Mazeiks (201250339, sc18rm@leeds.ac.uk)}

\title{\textsc{Comp2932}: Compiler Design and Construction -- Jack Compiler}
\maketitle
\today

University of Leeds School of Computing

## 1. Introduction
I have completed the entire compiler, including the lexical analysis, parsing, code generation and semantic analysis, as well as a command-line utility. I was able to reach the milestones on time and sometimes earlier.

The compiler is written in Java, using JUnit5 as the automated test platform and the Maven build automation tool. I have tried to make the code as clean as possible, maximizing code reuse by taking an object-oriented approach. I used IntelliJ IDEA on Linux (Linux is cool) for writing code and various workflows.

I have written automated tests with 70 test cases to ensure each of these features works properly; all these tests pass on my machine as well as on the school's computers. These can be found in the `test` directory. The program works on DEC10 to the best of my knowledge - it compiles without errors, and I have checked the main functionality.

## 2. The Lexical Analyser
The lexical analysis occurs in `lexical.Lexer` as well as some helper classes. The interface exposed to other classes (defined in `lexical.TokenSource`) consists of three methods, `getNextToken`, `peekNextToken`, and `hasNextToken`.

The lexer is implemented as a set of methods that consume a particular piece of the input. For example, `consumeEmpty` clears all the space characters and comments from the input until the start of the next token, and `consumeInteger` consumes and returns an integer literal token. The `consumeToken` method looks at the next character, determines the token category based on this, calls the appropriate `consume-` method based on this.

The input is processed character by character, determining whether it ends the current token or not based on the context.

Tokens are represented with the `lexical.data.Token` class, which contains the lexeme and token type, as well as information about where the token was found, such as the line and column number. The Lexer categorizes 8 different types of tokens (listed in `lexical.data.TokenType`):

- `SYMBOL`
- `KEYWORD`
- `IDENTIFIER`
- `LITERAL_BOOLEAN`
- `LITERAL_INTEGER`
- `LITERAL_STRING`
- `LITERAL_NULL`
- `EOF`

The lexer was thoroughly tested using unit tests (`LexerTest`), by running the lexer on test input strings and verifying that the output is as expected. The tests check if every token type is identified correctly, and verify that corner cases are handled correctly.

## 3. The Parser
The parser is implemented in the `syntactic.Parser` class, which accepts a `TokenSource` as input and produces a parse tree, removing some redundant information. It is implemented as a set of functions, each of which is responsible for parsing a particular type of node. The publicly exposed method, `parseProgram`, is responsible for parsing the entire program.

Whenever an unexpected token is encountered, and exception is thrown. The exception includes the unexpected token so that this can later be reported to the user.

The parser also has one basic unit test. However, writing unit tests for verifying that the tree is produced correctly was tedious, so the parser was mostly tested manually, by printing a visual representation of the tree to the console.

## 4. The Symbol Table
There was a separate symbol table for each scope. Each class had its own table of fields and a table of subroutines. Every subroutine also had a table of arguments and a table of variables.

Symbol tables were implemented as hashmaps of Strings to Symbols, allowing the compiler to look up any name efficiently.

To load the OS classes, I have added a `.jack` file for each of the OS classes, with empty subroutines corresponding to the Jack OS API. These OS files are tokenized and parsed, but instead of compilation, their pre-compiled (provided) counterparts are copied. Having the OS classes in the symbol table  allows me to easily perform semantic analysis on OS subroutine usage.

## 5. The Semantic Analyser
The semantic analysis was mostly done in the code generation step as well as the parsing step. Bellow is a summary of the semantic checks that were made.

### Declarations

- When parsing a class declaration, check that a class of the specified name hasn't already been declared by looking it up in the class symbol table.
- When parsing a subroutine declaration, check that a subroutine of the specified name hasn't already been declared in this class by looking it up in the subroutine symbol table for this class.
- When parsing a field declaration, check that a field of the specified name hasn't already been declared in this class by looking it up in the field symbol table for this class.

### Local Variables

- Check that all variable references are resolved by looking them up in symbol tables.
- Ensure that a local variable is not used before its declaration. Declaration is kept track of by a boolean flag in the variable.
- When parsing a local variable declaration, check that there is no variable or argument of the same name in the appropriate symbol tables.
- When parsing a variable as part of an expression, check that it has been initialized. Initialization is kept track of by a boolean flag in the variable.
- Ensure that arrays are initialized before accessing their members.

### Subroutine Calls

- Ensure that all subroutine calls resolve to a subroutine by looking them up in the symbol table when encountering a subroutine call.
- Ensure that instance methods are called on instances and not on classes by checking if they are static in the symbol table.

### Program Flow

- Ensure that all subroutines return in all possible code paths.
- Perform a basic check to ensure there is no dead code following a guaranteed return statement.

### Type Checking

- Ensure that `while` and `if` conditional expressions are boolean.
- Ensure that array index expressions are integer.
- Ensure that the type of an expression matches the type of the variable it is assigned to.
- Ensure that the type of a return expression matches the type of the function, and that void return statements are accepted in and only in void functions
- Ensure that the arguments of a subroutine call match in number and type with the required parameters
- Ensure that operands of expressions have a valid type (e.g. only allow the addition operator `+` for integer types)

## 6. Code Generation
The code generation step (occuring in the `vm` package) accepts the parse tree generated by the parser as input. It allocates space for all variables that are on the stack by ensuring they all have distinct indices where necesary. Then it iterates through all the subroutines and generates corresponding VM code for each of them. This is output to individuall `.vm` files by a helper class to the target directory. The provided OS `.vm` files are copied into this directory as well.

Each type of statement or expression has a decicated method for compiling it. Depending on the node type, the correct method is selected:

```$java
protected StaticSubroutineState compileStatement(Node node) throws SemanticException {
		if (node instanceof Variable) {
			return processVariableDeclaration((Variable) node);
		}

		switch (node.getNodeType()) {
			case NODE_GROUP:
				return compileBlock((CodeNode) node);
			case STATEMENT_RETURN:
				return compileReturnStatement((CodeNode) node);
			case STATEMENT_LET:
				compileLetStatement((CodeNode) node);
				break;
			case STATEMENT_IF:
				return compileIfStatement((CodeNode) node);
			case STATEMENT_WHILE:
				compileWhileStatement((CodeNode) node);
				break;
			case SUBROUTINE_CALL:
				compileSubroutineCall((CodeNode) node, true);
				break;
			default:
				throw new IllegalStateException("Unexpected value: " + node.getNodeType());
		}

		return new StaticSubroutineState();
	}
```

Any valid Jack program, as well as any Jack program with only minor semantic errors can be compiled without errors and successfully run on the emulator. This was tested with some of the provided sample programs.

## 7. Compiler Usage Instructions
Compilation is performed using Maven, and works on the Dec10 machines. I have tested this by using `ssh` to remotely access these machines. It should also work on any other machine with Maven 3 installed, though I have only tested this on Dec10 and locally.

Ensure that you are in the compiler's source code directory. You should see the `src` directory and the `pom.xml` file from your working directory.

To compile the program, run `mvn package`. Then, `cd target/classes` to navigate to the location where the classes are compiled. Run `java Compile <input> <output>` to compile a Jack program, where:

- `<input>` is a path to the Jack source code you wish to compile.
- `<output>` is the target directory. This can be omitted if you wish to output the `.vm` files in the same location as the source files. 

If a path contains whitespace, you have to surround it with quotes or escape the whitespace.

To run the automated tests, run `mvn test`.