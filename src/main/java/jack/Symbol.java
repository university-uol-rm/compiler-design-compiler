package jack;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Helper class storing jack symbols
 */
public class Symbol {
	private static final HashSet<String> symbols;

	static {
		symbols = new HashSet<>(Arrays.asList(
				"{", "}", "[", "]", "(", ")",
				"+", "-", "*", "/", "&", "|", "~", "<", ">",
				"=", ";", ",", "."
		));
	}

	public static HashSet<String> getSymbols() {
		return symbols;
	}
}
