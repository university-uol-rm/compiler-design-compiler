package jack;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Helper class storing Jack keywords
 */
public class Keyword {
	public static final String CLASS = "class";
	public static final String CONSTRUCTOR = "constructor";
	public static final String METHOD = "method";
	public static final String FUNCTION = "function";
	public static final String INT = "int";
	public static final String BOOLEAN = "boolean";
	public static final String CHAR = "char";
	public static final String VOID = "void";
	public static final String VAR = "var";
	public static final String STATIC = "static";
	public static final String FIELD = "field";
	public static final String LET = "let";
	public static final String DO = "do";
	public static final String IF = "if";
	public static final String ELSE = "else";
	public static final String WHILE = "while";
	public static final String RETURN = "return";
	public static final String THIS = "this";

	public static final String NULL = "null";
	public static final String TRUE = "true";
	public static final String FALSE = "false";

	private static final HashSet<String> keywords;
	private static final HashSet<String> booleanValues;
	private static final HashSet<String> functionKeywords;
	private static final HashSet<String> primitiveTypes;
	private static final HashSet<String> classVariableKeywords;

	static {
		keywords = new HashSet<>(Arrays.asList(CLASS,
				CONSTRUCTOR,
				METHOD,
				FUNCTION,
				INT,
				BOOLEAN,
				CHAR,
				VOID,
				VAR,
				STATIC,
				FIELD,
				LET,
				DO,
				IF,
				ELSE,
				WHILE,
				RETURN,
				THIS
		));
		booleanValues = new HashSet<>(Arrays.asList(
				TRUE, FALSE
		));

		functionKeywords = new HashSet<>(Arrays.asList(CONSTRUCTOR, FUNCTION, METHOD));
		primitiveTypes = new HashSet<>(Arrays.asList(INT, CHAR, BOOLEAN));
		classVariableKeywords = new HashSet<>(Arrays.asList(STATIC, FIELD));
	}

	public static HashSet<String> functionKeywords() {
		return functionKeywords;
	}

	public static HashSet<String> primitiveTypes() {
		return primitiveTypes;
	}

	public static HashSet<String> classVariableKeywords() {
		return classVariableKeywords;
	}

	public static HashSet<String> booleanValues() {
		return booleanValues;
	}

	public static HashSet<String> allKeywords() {
		return keywords;
	}
}
