package vm.reference;

/**
 * Enum of all Jack VM stacks
 */
public enum VmStack {
	STATIC("static"),
	ARGUMENT("argument"),
	LOCAL("local"),
	THIS("this"),
	THAT("that"),
	POINTER("pointer"),
	TEMP("temp"),
	CONST("constant");

	private final String name;

	VmStack(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
