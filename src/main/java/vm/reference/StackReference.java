package vm.reference;

import vm.type.Type;

/**
 * Represents a reference to a VM stack
 */
public class StackReference {
	private final VmStack vmStack;
	private final int offset;

	public StackReference(VmStack vmStack, int offset) {
		this.vmStack = vmStack;
		this.offset = offset;
	}

	public StackReference(VmStack vmStack) {
		this(vmStack, 0);
	}

	public VmStack getVmStack() {
		return vmStack;
	}

	public int getOffset() {
		return offset;
	}

	public String getName() {
		return String.format("<pointer to %s>[%d]", getVmStack().getName(), this.getOffset());
	}

	public Type getType() {
		return Type.ANY_TYPE;
	}
}
