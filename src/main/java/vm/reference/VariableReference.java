package vm.reference;

import syntactic.data.symbol.Variable;
import vm.type.Type;

/**
 * Represents a reference to a variable
 */
public class VariableReference extends StackReference {
	private final Variable variable;

	public VariableReference(VmStack vmStack, Variable variable) {
		super(vmStack);
		this.variable = variable;
	}

	public Variable getVariable() {
		return variable;
	}

	public int getOffset() {
		return variable.getIndex();
	}

	public Type getType() {
		return variable.getType();
	}

	public boolean isConstant() {
		return getVmStack() == VmStack.CONST;
	}

	public String getName() {
		return variable.getName();
	}
}
