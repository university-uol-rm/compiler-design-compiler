package vm.reference;

import vm.type.Type;

/**
 * Represents a VM constant
 */
public class ConstReference extends VariableReference {
	private final int value;
	private final Type type;

	public ConstReference(int value, Type type) {
		super(VmStack.CONST, null);
		this.value = value;
		this.type = type;
	}

	@Override
	public String getName() {
		String stringValue;

		switch (type.getName()) {
			case "char":
				stringValue = String.valueOf((char) value);
				break;
			case "boolean":
				stringValue = value == 0 ? "false" : "true";
				break;
			default:
				stringValue = String.valueOf(value);
		}
		return "Constant " + stringValue;
	}

	@Override
	public int getOffset() {
		return value;
	}

	public int getValue() {
		return value;
	}

	@Override
	public Type getType() {
		return type;
	}
}
