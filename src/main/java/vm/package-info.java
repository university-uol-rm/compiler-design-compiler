/**
 * Contains logic for generating VM code for a specified parse tree.
 */
package vm;