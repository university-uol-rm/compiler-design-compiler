package vm.type;

import static jack.Keyword.VOID;

/**
 * The void type
 */
public class VoidType extends Type {
	public static final Type VOID_TYPE = new VoidType();

	public VoidType() {
		super(VOID);
	}

	@Override
	public boolean accepts(Type other) {
		throw new RuntimeException("Absurd `accepts` check: void types cannot belong to variables or parameters");
	}

	@Override
	public boolean isVoid() {
		return true;
	}
}
