package vm.type;

import java.util.Objects;

import static jack.Keyword.*;

/**
 * Jack compile-time type
 */
public class Type {
	public static final String ANY_TYPE_NAME = "ANYTYPE";
	public static final String ARRAY_TYPE_NAME = "Array";
	public static final String INT_TYPE_NAME = "int";
	public static final String CHAR_TYPE_NAME = "char";

	public static final Type INT_TYPE = new Type(INT);
	public static final Type BOOLEAN_TYPE = new Type(BOOLEAN);
	public static final Type CHAR_TYPE = new Type(CHAR);
	public static final Type NULL_TYPE = new Type(NULL);
	public static final Type ARRAY_TYPE = new Type(ARRAY_TYPE_NAME);
	public static final Type ANY_TYPE = new Type(ANY_TYPE_NAME);

	private final String name;

	public Type(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public boolean accepts(Type other) {
		if (isAnyType()) return true;
		if (other.isAnyType()) return true;

		if (other.isNullType()) return true;

		if (isChar() && other.isInt()) return true;
		if (isInt() && other.isChar()) return true;

		return getName().equals(
				other.getName()
		);
	}

	private boolean isChar() {
		return name.equals(CHAR_TYPE_NAME);
	}

	private boolean isInt() {
		return name.equals(INT_TYPE_NAME);
	}

	public boolean isAnyType() {
		return getName().equals(ANY_TYPE_NAME);
	}

	public boolean isNullType() {
		return getName().equals(NULL);
	}

	public boolean isVoid() {
		return false;
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Type type = (Type) o;
		return name.equals(type.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	public boolean isSameAs(Type other) {
		if (isAnyType()) return true;
		if (other.isAnyType()) return true;

		return getName().equals(
				other.getName()
		);
	}
}
