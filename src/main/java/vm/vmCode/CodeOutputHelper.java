package vm.vmCode;

import syntactic.data.JackClass;
import syntactic.data.symbol.Subroutine;
import syntactic.data.symbol.Variable;
import vm.operation.Operation;
import vm.operation.UnaryOperation;
import vm.reference.StackReference;
import vm.reference.VmStack;
import vm.type.Type;

/**
 * Wrapper for CodeOutput, with methods for each VM instruction
 */
public class CodeOutputHelper {
	public static final String STATIC = "static";
	public static final String ARGUMENT = "argument";
	public static final String LOCAL = "local";
	public static final String THIS = "this";
	public static final String THAT = "that";
	public static final String POINTER = "pointer";
	public static final String TEMP = "temp";
	public static final String CONST = "const";

	private final CodeOutput output;

	public CodeOutputHelper(CodeOutput output) {
		this.output = output;
	}

	protected String pop(String segment, int index) {
		return String.format("pop %s %d", segment, index);
	}

	protected String push(String segment, int index) {
		return String.format("push %s %d", segment, index);
	}

	public void declareSubroutine(JackClass jackClass, Subroutine subroutine, int variableSize) {
		output.appendInstruction(
				String.format("function %s %d", subroutine.getVmIdentifier(), variableSize),
				String.format("%s in class %s", subroutine.toString(), jackClass.getName())
		);
	}

	public void call(Subroutine subroutine) {
		int additionalParams = subroutine.isStatic() ? 0 : 1;
		output.appendInstruction(
				String.format("call %s %d", subroutine.getVmIdentifier(), subroutine.getOrderedParameters().size() + additionalParams),
				null
		);
	}

	public void returnInstruction() {
		output.appendInstruction("return", null);
	}

	public void pushReference(StackReference reference) {
		String comment = reference.getName();

		VmStack stack = reference.getVmStack();

		if (stack.equals(VmStack.CONST) && reference.getOffset() < 0) {
			output.appendInstruction(
					push(
							stack.getName(),
							-reference.getOffset()
					),
					comment
			);
			operation(UnaryOperation.INT_NEGATION);
			return;
		}

		if (Type.INT_TYPE.accepts(reference.getType()) && stack.equals(VmStack.CONST))
			comment = null;

		output.appendInstruction(
				push(
						stack.getName(),
						reference.getOffset()
				),
				comment
		);
	}

	public void popReference(StackReference reference) {
		output.appendInstruction(
				pop(
						reference.getVmStack().getName(),
						reference.getOffset()
				),
				String.format("Assign to %s", reference.getName())
		);
	}

	public void pushField(String objectIdentifier, Variable field) {
		output.appendInstruction(
				push(THAT, field.getIndex()),
				String.format("%s.%s", objectIdentifier, field.getName())
		);
	}

	public void pushStatic(String classIdentifier, Variable field) {
		output.appendInstruction(
				push(STATIC, field.getIndex()),
				String.format("%s.%s", classIdentifier, field.getName())
		);
	}

	public void setThis(String comment) {
		output.appendInstruction(
				"pop pointer 0",
				comment
		);
	}

	public void saveThis(String comment) {
		output.appendInstruction(
				"push pointer 0",
				comment
		);
	}

	public void setThat(String comment) {
		output.appendInstruction(
				pop(POINTER, 1),
				comment
		);
	}

	public void ignore() {
		output.appendInstruction(
				pop(TEMP, 0),
				"Ignore value"
		);
	}

	public void operation(Operation operation) {
		output.appendInstruction(
				operation.getInstruction(),
				operation.getName()
		);
	}

	public void ifGoto(String label) {
		output.appendInstruction(
				String.format("if-goto %s", label),
				null
		);
	}

	public void label(String label) {
		output.appendInstruction(
				String.format("label %s", label),
				null
		);
	}

	public void gotoLabel(String label) {
		output.appendInstruction(
				String.format("goto %s", label),
				null
		);
	}
}
