package vm.vmCode;

/**
 * Capable of outputing code
 */
public interface CodeOutput {
	void appendInstruction(String instruction, String comment);
}
