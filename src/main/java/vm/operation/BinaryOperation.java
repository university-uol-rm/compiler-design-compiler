package vm.operation;

import vm.type.Type;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents a binary operation that can be compiled to VM
 */
public enum BinaryOperation implements Operation {
	AND("and", "logical and", Type.BOOLEAN_TYPE, Type.BOOLEAN_TYPE, Type.INT_TYPE),
	OR("or", "logical or", Type.BOOLEAN_TYPE, Type.BOOLEAN_TYPE, Type.INT_TYPE),
	EQUALS("eq", "equality check", Type.BOOLEAN_TYPE, Type.ANY_TYPE),
	LESS("lt", "less than", Type.BOOLEAN_TYPE, Type.INT_TYPE),
	GREATER("gt", "greater than", Type.BOOLEAN_TYPE, Type.INT_TYPE),
	ADD("add", "add", Type.INT_TYPE, Type.INT_TYPE),
	SUBTRACT("sub", "subtract", Type.INT_TYPE, Type.INT_TYPE),
	MULTIPLY(null, "multiply", Type.INT_TYPE, Type.INT_TYPE),
	DIVIDE(null, "divide", Type.INT_TYPE, Type.INT_TYPE),
	;


	private final String instruction;
	private final String name;
	private final Type outputType;
	private final Set<Type> inputTypes;

	BinaryOperation(String instruction, String name, Type outputType, Type... inputTypes) {
		this.instruction = instruction;
		this.name = name;
		this.outputType = outputType;
		this.inputTypes = new HashSet<>(Arrays.asList(inputTypes));
	}

	@Override
	public String getInstruction() {
		return instruction;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Type getOutputType() {
		return outputType;
	}

	@Override
	public boolean accepts(Type type) {
		return inputTypes.stream().anyMatch(item -> item.accepts(type));
	}
}
