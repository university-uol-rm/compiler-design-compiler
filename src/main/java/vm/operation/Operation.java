package vm.operation;

import vm.type.Type;

/**
 * Represents an operation that can be compiled to VM
 */
public interface Operation {
	String getInstruction();

	boolean accepts(Type type);

	Type getOutputType();

	String getName();
}
