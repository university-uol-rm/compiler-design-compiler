package vm.operation;

import vm.type.Type;

/**
 * Represents a unary operation that can be compiled to VM
 */
public enum UnaryOperation implements Operation {
	INT_NEGATION("neg", Type.INT_TYPE, "integer negation"),
	BINARY_NEGATION("not", Type.BOOLEAN_TYPE, "boolean negation");

	private final String instruction;
	/**
	 * Input and output type of this operation
	 */
	private final Type type;
	private final String name;

	UnaryOperation(String instruction, Type type, String name) {
		this.instruction = instruction;
		this.type = type;
		this.name = name;
	}

	public String getInstruction() {
		return instruction;
	}

	@Override
	public boolean accepts(Type type) {
		return this.type.accepts(type);
	}

	@Override
	public Type getOutputType() {
		return this.type;
	}

	@Override
	public String getName() {
		return name;
	}
}
