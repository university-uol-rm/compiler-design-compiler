package vm.generator;

import semantic.SemanticException;
import semantic.WarningReporter;
import syntactic.data.*;
import syntactic.data.symbol.Subroutine;
import syntactic.data.symbol.Variable;
import vm.reference.VariableReference;
import vm.reference.VmStack;
import vm.type.Type;
import vm.vmCode.CodeOutputHelper;

/**
 * Responsible for resolving subroutines and variables
 */
public class Resolver {
	public static final String STD_CLASS_MATH = "Math";
	public static final String STD_FUNCTION_MATH_MULTIPLY = "multiply";
	public static final String STD_FUNCTION_MATH_DIVIDE = "divide";

	public static final String STD_CLASS_STRING = "String";
	public static final String STD_FUNCTION_STRING_NEW = "new";
	public static final String STD_FUNCTION_STRING_APPEND = "appendChar";

	public static final String STD_CLASS_MEMORY = "Memory";
	public static final String STD_FUNCTION_MEMORY_ALLOCATE = "alloc";


	final Program program;
	final JackClass currentClass;
	final VariableTable parameters;
	final VariableTable variables;
	private final WarningReporter reporter;

	final CodeOutputHelper output;

	public Resolver(CodeOutputHelper output, Program program, JackClass currentClass, VariableTable parameters, VariableTable variables, WarningReporter reporter) {
		this.output = output;
		this.program = program;
		this.currentClass = currentClass;
		this.parameters = parameters;
		this.variables = variables;
		this.reporter = reporter;
	}

	public Type pushIdentifier(CodeNode node) throws SemanticException {
		VariableReference reference = getIdentifier(node);
		if (!reference.getVariable().isInitialized())
			reporter.handle(new SemanticException("Cannot read value of uninitialized variable", node));

		output.pushReference(reference);
		return reference.getType();
	}

	public VariableReference getIdentifier(CodeNode node) throws SemanticException {
		String identifier = node.getSource().getLexeme();

		if (parameters.containsKey(identifier)) {
			Variable parameter = parameters.get(identifier);
			return new VariableReference(VmStack.ARGUMENT, parameter);

		} else if (variables.containsKey(identifier)) {
			Variable variable = variables.get(identifier);
			if (!variable.isInScope())
				throw new SemanticException("Variable has not been declared yet", node);

			return new VariableReference(VmStack.LOCAL, variable);

		} else if (currentClass.containsField(identifier)) {
			Variable field = currentClass.getField(identifier);
			return new VariableReference(field.isStatic() ? VmStack.STATIC : VmStack.THIS, field);

		} else {
			throw new SemanticException("Unresolved variable", node);
		}
	}

	public Type pushIdentifierPath(CodeNode expression) throws SemanticException {
		CodeNode object = (CodeNode) expression.getChild(0);
		String objectIdentifier = object.getSource().getLexeme();

		CodeNode attribute = (CodeNode) expression.getChild(1);
		String attributeIdentifier = attribute.getSource().getLexeme();

		if (program.containsJackClass(objectIdentifier)) {
			JackClass jackClass = program.getJackClass(objectIdentifier);
			return pushStaticField(jackClass, objectIdentifier, attributeIdentifier, attribute);
		} else {
			Type type = pushIdentifier(object);
			output.setThat(
					String.format("Set `that` to object %s to access attribute %s", objectIdentifier, attributeIdentifier)
			);

			if (!program.containsJackClass(type.getName()))
				throw new SemanticException(
						String.format("Primitive %s object %s has no attribute %s", type, objectIdentifier, attributeIdentifier),
						attribute
				);

			JackClass jackClass = program.getJackClass(type.getName());

			return pushInstanceField(jackClass, objectIdentifier, attributeIdentifier, attribute);
		}
	}

	private Type pushInstanceField(JackClass jackClass, String objectIdentifier, String attributeIdentifier, CodeNode attribute) throws SemanticException {
		Variable field = getField(jackClass, attributeIdentifier, attribute);

		if (field.isStatic())
			throw new SemanticException(
					String.format("Field %s in class %s is static and cannot be accessed from an instance", attributeIdentifier, objectIdentifier),
					attribute
			);

		output.pushField(objectIdentifier, field);

		return field.getType();
	}

	private Type pushStaticField(JackClass jackClass, String classIdentifier, String attributeIdentifier, CodeNode attribute) throws SemanticException {
		Variable field = getField(jackClass, attributeIdentifier, attribute);

		if (!field.isStatic())
			throw new SemanticException(
					String.format("Field %s in class %s is not static and can only be accessed from an instance", attributeIdentifier, classIdentifier),
					attribute
			);

		output.pushStatic(classIdentifier, field);

		return field.getType();
	}

	private Variable getField(JackClass jackClass, String attributeIdentifier, CodeNode attribute) throws SemanticException {
		if (!jackClass.containsField(attributeIdentifier))
			throw new SemanticException(
					String.format("Class %s has no attribute named %s", jackClass.getName(), attributeIdentifier),
					attribute
			);

		return jackClass.getField(attributeIdentifier);
	}

	public Subroutine resolveSubroutine(CodeNode identifier) throws SemanticException {
		if (identifier.getNodeType() == NodeType.IDENTIFIER) {
			return resolveSameClassSubroutine(identifier);
		}

		if (identifier.getNodeType() == NodeType.IDENTIFIER_PATH) {
			CodeNode object = (CodeNode) identifier.getChild(0);
			String objectIdentifier = object.getSource().getLexeme();

			CodeNode attribute = (CodeNode) identifier.getChild(1);
			String subroutineIdentifier = attribute.getSource().getLexeme();

			if (program.containsJackClass(objectIdentifier)) {
				return resolveStaticSubroutine(objectIdentifier, attribute);
			} else {
				return resolveInstanceSubroutine(object, attribute);
			}
		}

		throw new IllegalStateException("Invalid node type: " + identifier.getNodeType());
	}

	private Subroutine resolveInstanceSubroutine(CodeNode object, CodeNode methodNode) throws SemanticException {
		Type type = pushIdentifier(object);

		if (!program.containsJackClass(type.getName()))
			throw new SemanticException(
					String.format("Primitive type %s has no methods", type),
					methodNode
			);

		JackClass jackClass = program.getJackClass(type.getName());
		Subroutine method = getSubroutine(jackClass, methodNode);
		ensureInstance(method, methodNode);

		return method;
	}

	private Subroutine resolveStaticSubroutine(String classIdentifier, CodeNode functionNode) throws SemanticException {
		JackClass jackClass = program.getJackClass(classIdentifier);
		Subroutine function = getSubroutine(jackClass, functionNode);
		ensureStatic(functionNode, function);

		return function;
	}

	private Subroutine resolveSameClassSubroutine(CodeNode identifier) throws SemanticException {
		String subroutineName = identifier.getSource().getLexeme();

		if (!currentClass.containsSubroutine(subroutineName))
			throw new SemanticException(
					String.format("The class %s has no method %s", currentClass.getName(), subroutineName),
					identifier
			);

		Subroutine method = currentClass.getSubroutine(subroutineName);
		ensureInstance(method, identifier);

		output.saveThis(String.format("Pass this to method %s", method.getName()));

		return method;
	}

	private void ensureStatic(CodeNode attribute, Subroutine function) throws SemanticException {
		if (!function.isStatic())
			throw new SemanticException(
					String.format("The method %s is not static and must be called on an instance", function.getName()),
					attribute
			);
	}

	private void ensureInstance(Subroutine subroutine, CodeNode identifier) throws SemanticException {
		if (subroutine.isStatic())
			throw new SemanticException(
					String.format("The function %s is static and must be called on the class", subroutine.getName()),
					identifier
			);
	}

	private Subroutine getSubroutine(JackClass jackClass, CodeNode attribute) throws SemanticException {
		String identifier = attribute.getSource().getLexeme();

		if (!jackClass.containsSubroutine(identifier))
			throw new SemanticException(
					String.format("Class %s has no subroutine %s", jackClass.getName(), identifier),
					attribute
			);

		return jackClass.getSubroutine(identifier);
	}

	public Type pushThis() {
		output.saveThis("this");
		return new Type(currentClass.getName());
	}

	public Subroutine getMultiplySubroutine() {
		return program
				.getJackClass(STD_CLASS_MATH)
				.getSubroutine(STD_FUNCTION_MATH_MULTIPLY);
	}

	public Subroutine getDivideSubroutine() {
		return program
				.getJackClass(STD_CLASS_MATH)
				.getSubroutine(STD_FUNCTION_MATH_DIVIDE);
	}

	public Subroutine getStringConstructor() {
		return program
				.getJackClass(STD_CLASS_STRING)
				.getSubroutine(STD_FUNCTION_STRING_NEW);
	}

	public Subroutine getStringAppend() {
		return program
				.getJackClass(STD_CLASS_STRING)
				.getSubroutine(STD_FUNCTION_STRING_APPEND);
	}

	public Subroutine getMemoryAllocate() {
		return program
				.getJackClass(STD_CLASS_MEMORY)
				.getSubroutine(STD_FUNCTION_MEMORY_ALLOCATE);
	}

	public JackClass getCurrentClass() {
		return currentClass;
	}
}
