package vm.generator;

import jack.Keyword;
import lexical.data.Token;
import lexical.data.TokenType;
import semantic.SemanticException;
import semantic.WarningReporter;
import syntactic.data.CodeNode;
import syntactic.data.Node;
import syntactic.data.NodeType;
import syntactic.data.symbol.Parameter;
import syntactic.data.symbol.Subroutine;
import syntactic.data.symbol.Variable;
import vm.identifier.IfLabels;
import vm.identifier.WhileLabels;
import vm.operation.BinaryOperation;
import vm.operation.UnaryOperation;
import vm.reference.ConstReference;
import vm.reference.StackReference;
import vm.reference.VariableReference;
import vm.reference.VmStack;
import vm.type.Type;
import vm.type.VoidType;
import vm.vmCode.CodeOutputHelper;

import java.util.List;

/**
 * Outputs VM code for a particular subroutine
 */
public class SubroutineCompiler {
	private final Resolver resolver;
	private final CodeOutputHelper output;
	private final Subroutine subroutine;
	private final WarningReporter reporter;
	private int ifIndex = 0, whileIndex = 0;

	public SubroutineCompiler(Resolver resolver, CodeOutputHelper output, Subroutine subroutine, WarningReporter reporter) {
		this.resolver = resolver;
		this.output = output;
		this.subroutine = subroutine;
		this.reporter = reporter;
	}

	protected int getNextIfIndex() {
		return ifIndex++;
	}

	protected int getNextWhileIndex() {
		return whileIndex++;
	}

	public void compileBody() throws SemanticException {
		if (subroutine.isConstructor()) {
			int size = resolver.getCurrentClass().getSize();
			output.pushReference(new ConstReference(size, Type.INT_TYPE));
			output.call(resolver.getMemoryAllocate());
			output.setThis("Set `this` to newly created object");
		}

		StaticSubroutineState state = compileBlock(subroutine.getBody());

		if (!state.hasTerminated())
			throw new SemanticException("Not all possible code paths terminate", subroutine.getBody());
	}

	protected StaticSubroutineState compileBlock(CodeNode code) throws SemanticException {
		List<Node> children = code.getChildren();
		StaticSubroutineState state = new StaticSubroutineState();

		for (Node node : children) {
			if (state.hasTerminated())
				throw new SemanticException("Detected unreachable statement", node);

			StaticSubroutineState childState = compileStatement(node);

			if (childState.hasTerminated())
				state.markTerminated();
		}

		return state;
	}

	protected StaticSubroutineState compileStatement(Node node) throws SemanticException {
		if (node instanceof Variable) {
			return processVariableDeclaration((Variable) node);
		}

		switch (node.getNodeType()) {
			case NODE_GROUP:
				return compileBlock((CodeNode) node);
			case STATEMENT_RETURN:
				return compileReturnStatement((CodeNode) node);
			case STATEMENT_LET:
				compileLetStatement((CodeNode) node);
				break;
			case STATEMENT_IF:
				return compileIfStatement((CodeNode) node);
			case STATEMENT_WHILE:
				compileWhileStatement((CodeNode) node);
				break;
			case SUBROUTINE_CALL:
				compileSubroutineCall((CodeNode) node, true);
				break;
			default:
				throw new IllegalStateException("Unexpected value: " + node.getNodeType());
		}

		return new StaticSubroutineState();
	}

	protected void compileWhileStatement(CodeNode whileStatement) throws SemanticException {
		WhileLabels whileLabels = new WhileLabels(subroutine, getNextWhileIndex());

		Node expression = whileStatement.getChild(0);
		Node body = whileStatement.getChild(1);

		output.label(whileLabels.getStartLabel());
		Type type = pushExpression((CodeNode) expression);
		if (!Type.BOOLEAN_TYPE.accepts(type))
			reporter.handle(new SemanticException(
					String.format("While statement conditional must be Boolean, %s found", type.getName()),
					expression
			));
		output.operation(UnaryOperation.BINARY_NEGATION);
		output.ifGoto(whileLabels.getEndLabel());
		compileBlock((CodeNode) body);
		output.gotoLabel(whileLabels.getStartLabel());
		output.label(whileLabels.getEndLabel());
	}

	protected StaticSubroutineState compileIfStatement(CodeNode ifStatement) throws SemanticException {
		StaticSubroutineState state = new StaticSubroutineState(false);
		StaticSubroutineState elseState = null;
		IfLabels labels = new IfLabels(subroutine, getNextIfIndex());

		List<Node> children = ifStatement.getChildren();
		Node expression = children.get(0);
		Node ifBody = children.get(1);

		Type type = pushExpression((CodeNode) expression);
		if (!Type.BOOLEAN_TYPE.accepts(type))
			reporter.handle(new SemanticException(
					String.format("Conditional expression must be Boolean, %s found", type.getName()),
					expression
			));

		if (children.size() == 2) {
			output.operation(UnaryOperation.BINARY_NEGATION);
			output.ifGoto(labels.getEndLabel());
		} else {
			Node elseBody = children.get(2);

			output.ifGoto(labels.getBodyLabel());
			elseState = compileBlock((CodeNode) elseBody);
			output.gotoLabel(labels.getEndLabel());
			output.label(labels.getBodyLabel());
		}

		StaticSubroutineState ifState = compileBlock((CodeNode) ifBody);
		output.label(labels.getEndLabel());

		if (elseState != null)
			state = StaticSubroutineState.leastGuarantee(ifState, elseState);

		return state;
	}

	protected void compileLetStatement(CodeNode node) throws SemanticException {
		Node variable = node.getChild(0);
		Node expression = node.getChild(1);

		Type expressionType = pushExpression((CodeNode) expression);

		StackReference reference;
		if (variable.getNodeType() == NodeType.ARRAY_PATH) {
			CodeNode arrayTarget = (CodeNode) variable;
			CodeNode arrayNode = (CodeNode) arrayTarget.getChild(0);
			CodeNode indexNode = (CodeNode) arrayTarget.getChild(1);

			VariableReference array = resolver.getIdentifier(arrayNode);
			if (!array.getVariable().isInitialized())
				throw new SemanticException("Cannot set array member of uninitialized array", arrayNode);
			if (!array.getType().accepts(Type.ARRAY_TYPE))
				throw new SemanticException("Cannot set array member of non-array", arrayNode);

			Type indexType = pushExpression(indexNode);
			if (!Type.INT_TYPE.accepts(indexType))
				throw new SemanticException("Array index must be an integer", indexNode);

			output.pushReference(array);

			// add array index to array pointer
			output.operation(BinaryOperation.ADD);
			output.setThat(String.format(
					"Update `that` to set value of array %s member",
					array.getName()
			));

			reference = new StackReference(VmStack.THAT);
		} else {
			VariableReference variableReference = resolver.getIdentifier((CodeNode) variable);
			variableReference.getVariable().markInitialized();
			reference = variableReference;

			Type variableType = reference.getType();
			if (!variableType.accepts(expressionType))
				reporter.handle(new SemanticException(
						String.format("Value of type %s assigned to variable of type %s", expressionType, variableType),
						node
				));
		}

		output.popReference(reference);
	}

	protected StaticSubroutineState compileReturnStatement(CodeNode statement) throws SemanticException {
		Type requiredType = subroutine.getType();

		if (statement.childCount() == 0) {
			if (!requiredType.isVoid())
				throw new SemanticException("Encountered return statement with no value in non-void subroutine", statement);
			output.pushReference(new ConstReference(0, Type.NULL_TYPE));
		} else {
			if (requiredType.isVoid())
				throw new SemanticException("Cannot return a value in void subroutine", statement);

			Type actualType = pushExpression((CodeNode) statement.getChild(0));

			if (!requiredType.accepts(actualType))
				reporter.handle(new SemanticException(String.format("Expected type %s, %s returned instead", requiredType, actualType), statement));
		}

		output.returnInstruction();

		return new StaticSubroutineState(true);
	}

	protected Type compileSubroutineCall(CodeNode expression, boolean ignoreValue) throws SemanticException {
		Node identifier = expression.getChild(0);
		CodeNode parameters = (CodeNode) expression.getChild(1);

		Subroutine subroutine = resolver.resolveSubroutine((CodeNode) identifier);

		compileArguments(parameters.getChildren(), subroutine.getOrderedParameters());
		output.call(subroutine);

		if (!subroutine.getType().isVoid() && ignoreValue) {
			return VoidType.VOID_TYPE;
		}

		if (ignoreValue) {
			output.ignore();
			return VoidType.VOID_TYPE;
		}

		return subroutine.getType();
	}

	protected void compileArguments(List<Node> arguments, List<Parameter> parameters) throws SemanticException {
		if (parameters.size() != arguments.size())
			throw new SemanticException(
					String.format("%d arguments given; %d required", arguments.size(), parameters.size()),
					arguments.get(0)
			);

		for (int i = 0; i < arguments.size(); i++) {
			Node argument = arguments.get(i);
			Type suppliedType = pushExpression((CodeNode) argument);
			Type requiredType = parameters.get(i).getType();

			if (!requiredType.accepts(suppliedType))
				reporter.handle(new SemanticException(
						String.format("Parameter %d requires type %s, %s supplied", i + 1, requiredType, suppliedType),
						argument
				));
		}
	}

	protected Type pushExpression(CodeNode expression) throws SemanticException {
		switch (expression.getNodeType()) {
			case SUBROUTINE_CALL:
				return compileSubroutineCall(expression, false);
			case EXPRESSION:
			case EXPRESSION_RELATIONAL:
			case EXPRESSION_ARITHMETIC:
			case EXPRESSION_TERM:
				return pushBinaryOperation(expression);
			case EXPRESSION_FACTOR:
				return pushUnaryOperation(expression);
			case EXPRESSION_LITERAL:
				return pushLiteral(expression);
			case EXPRESSION_THIS:
				return resolver.pushThis();
			case IDENTIFIER:
				return resolver.pushIdentifier(expression);
			case IDENTIFIER_PATH:
				return resolver.pushIdentifierPath(expression);
			case ARRAY_PATH:
				return pushArrayPath(expression);
			default:
				throw new IllegalStateException("Unexpected value: " + expression.getNodeType());
		}
	}

	protected Type pushArrayPath(CodeNode path) throws SemanticException {
		Node arrayExpression = path.getChild(0);
		Node indexExpression = path.getChild(1);

		Type arrayType = pushExpression((CodeNode) arrayExpression);
		if (!Type.ARRAY_TYPE.accepts(arrayType))
			throw new SemanticException("Cannot access array member of non-array", path);

		Type indexType = pushExpression((CodeNode) indexExpression);
		if (!Type.INT_TYPE.accepts(indexType))
			throw new SemanticException("Array index must be of integer type", indexExpression);

		output.operation(BinaryOperation.ADD);
		output.setThat("Update `that` pointer for array access");
		output.pushReference(new StackReference(VmStack.THAT));

		return Type.ANY_TYPE;
	}

	protected Type pushLiteral(CodeNode expression) {
		Token token = expression.getSource();

		if (token.getType() == TokenType.LITERAL_STRING) {
			return pushStringLiteral(token);
		}

		VariableReference constant = parseLiteral(token);
		output.pushReference(constant);

		return constant.getType();
	}

	protected String getStringValue(Token token) {
		String lexeme = token.getLexeme();
		return lexeme.substring(1, lexeme.length() - 1);
	}

	protected Type pushStringLiteral(Token token) {
		String value = getStringValue(token);

		output.pushReference(new ConstReference(
				value.length(),
				Type.INT_TYPE
		));
		output.call(resolver.getStringConstructor());

		// append string characters one by one
		for (int i = 0; i < value.length(); i++) {
			output.pushReference(new ConstReference(
					value.codePointAt(i),
					Type.CHAR_TYPE
			));

			output.call(resolver.getStringAppend());
		}

		return new Type("String");
	}

	protected VariableReference parseLiteral(Token token) {
		String lexeme = token.getLexeme();

		switch (token.getType()) {
			case LITERAL_BOOLEAN:
				boolean booleanValue = lexeme.equals(Keyword.TRUE);
				return new ConstReference(booleanValue ? -1 : 0, Type.BOOLEAN_TYPE);
			case LITERAL_INTEGER:
				int intValue = Integer.parseInt(lexeme);
				return new ConstReference(intValue, Type.INT_TYPE);
			case LITERAL_NULL:
				return new ConstReference(0, Type.NULL_TYPE);
			default:
				throw new IllegalStateException("Unexpected token while parsing literal: " + token.getType());
		}
	}

	protected Type pushUnaryOperation(CodeNode expression) throws SemanticException {
		Token source = expression.getSource();
		Type type = pushExpression((CodeNode) expression.getChild(0));
		String operator = source.getLexeme();

		UnaryOperation operation;
		if (operator.equals("-")) {
			operation = UnaryOperation.INT_NEGATION;
		} else if (operator.equals("~")) {
			operation = UnaryOperation.BINARY_NEGATION;
		} else {
			throw new SemanticException("Cannot compile invalid unary operation", expression);
		}

		if (!operation.accepts(type))
			throw new SemanticException(String.format(
					"Operator %s cannot be applied to type %s",
					operation.getName(),
					type.getName()
			), expression);

		output.operation(operation);

		return operation.getOutputType();
	}

	protected Type pushBinaryOperation(CodeNode expression) throws SemanticException {
		Type type = pushExpression((CodeNode) expression.getChild(0));
		Type type2 = pushExpression((CodeNode) expression.getChild(1));

		String operationLexeme = expression.getSource().getLexeme();

		BinaryOperation operation;

		switch (operationLexeme) {
			case "&":
				operation = BinaryOperation.AND;
				break;
			case "|":
				operation = BinaryOperation.OR;
				break;
			case "=":
				operation = BinaryOperation.EQUALS;
				break;
			case "<":
				operation = BinaryOperation.LESS;
				break;
			case ">":
				operation = BinaryOperation.GREATER;
				break;
			case "-":
				operation = BinaryOperation.SUBTRACT;
				break;
			case "+":
				operation = BinaryOperation.ADD;
				break;
			case "*":
				operation = BinaryOperation.MULTIPLY;
				break;
			case "/":
				operation = BinaryOperation.DIVIDE;
				break;
			default:
				throw new IllegalStateException("Unexpected operation: " + operationLexeme);
		}

		if (!operation.accepts(type))
			throw new SemanticException(
					String.format(
							"Invalid operand type %s for operation %s.",
							type.getName(),
							operation.getName()
					),
					expression.getChild(0)
			);

		if (!operation.equals(BinaryOperation.EQUALS)) {
			if (!type.isSameAs(type2))
				throw new SemanticException("Operand types must be the same", expression);
		}

		switch (operation) {
			case MULTIPLY:
				output.call(resolver.getMultiplySubroutine());
				break;
			case DIVIDE:
				output.call(resolver.getDivideSubroutine());
				break;
			default:
				output.operation(operation);
		}

		return operation.getOutputType();
	}

	protected StaticSubroutineState processVariableDeclaration(Variable variable) {
		variable.setInScope(true);
		return new StaticSubroutineState(false);
	}
}
