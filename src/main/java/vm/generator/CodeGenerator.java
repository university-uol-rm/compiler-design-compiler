package vm.generator;

import semantic.SemanticException;
import semantic.WarningReporter;
import syntactic.data.*;
import syntactic.data.symbol.Subroutine;
import syntactic.data.symbol.Variable;
import vm.identifier.VmIdentifiers;
import vm.reference.StackReference;
import vm.reference.VmStack;
import vm.vmCode.CodeOutput;
import vm.vmCode.CodeOutputHelper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Generates VM code from a parse tree
 */
public class CodeGenerator {
	private final Program program;
	private final WarningReporter reporter;

	public CodeGenerator(Program program, WarningReporter reporter) {
		this.program = program;
		this.reporter = reporter;
	}

	public void preprocess() {
		allocateFields();
		identifySubroutines();
	}

	public void compileClass(JackClass jackClass, CodeOutput codeOutput) throws SemanticException, FileNotFoundException {
		CodeOutputHelper output = new CodeOutputHelper(codeOutput);

		if (jackClass.isPrecompiled()) {
			File precompiled = jackClass.getPrecompiled();
			BufferedReader reader = new BufferedReader(new FileReader(precompiled));
			reader.lines().forEachOrdered(line -> codeOutput.appendInstruction(line, null));
			return;
		}

		for (Subroutine subroutine : jackClass.getSubroutines()) {
			VariableTable variables = new VariableTable();
			parseVariables(subroutine.getBody(), variables, subroutine);

			boolean hasThisParameter = !subroutine.isStatic();

			allocateVariables(variables.values(), 0);
			allocateVariables(subroutine.getOrderedParameters(), hasThisParameter ? 1 : 0);

			output.declareSubroutine(jackClass, subroutine, variables.size() + (hasThisParameter ? 1 : 0));

			Resolver resolver = new Resolver(
					output,
					program,
					jackClass,
					subroutine.getParameters(),
					variables,
					reporter
			);

			SubroutineCompiler subroutineCompiler = new SubroutineCompiler(resolver, output, subroutine, reporter);

			if (hasThisParameter) {
				output.pushReference(new StackReference(VmStack.ARGUMENT, 0));
				output.setThis("Update `this`");
			}
			subroutineCompiler.compileBody();
		}

	}

	private void identifySubroutines() {
		for (JackClass jackClass : program.getClasses()) {
			for (Subroutine subroutine : jackClass.getSubroutines()) {
				String vmIdentifier = VmIdentifiers.identify(jackClass, subroutine);
				subroutine.setVmIdentifier(vmIdentifier);
			}
		}
	}

	private void allocateVariables(Iterable<? extends Variable> variables, int startIndex) {
		int index = startIndex;
		for (Variable variable : variables) {
			variable.setIndex(index);
			index += variable.getSize();
		}
	}

	private void parseVariables(Node node, VariableTable variables, Subroutine subroutine) throws SemanticException {
		if (node instanceof Variable) {
			Variable variable = (Variable) node;

			if (subroutine.getParameters().containsKey(variable.getName()))
				throw new SemanticException("Variable name conflicts with existing parameter", variable);

			variables.register(variable.getName(), variable);
			return;
		}

		if (node instanceof CodeNode) {
			CodeNode codeNode = (CodeNode) node;

			for (Node child : codeNode.getChildren()) {
				parseVariables(child, variables, subroutine);
			}
		}
	}

	private void allocateFields() {
		int staticIndex = 0;
		for (JackClass jackClass : program.getClasses()) {
			int classIndex = 0;
			for (Variable field : jackClass.getFields()) {
				if (field.isStatic()) {
					field.setIndex(staticIndex);
					staticIndex += field.getSize();
				} else {
					field.setIndex(classIndex);
					field.setInScope(true);
					classIndex += field.getSize();
				}
			}
		}
	}

	public Program getProgram() {
		return program;
	}
}
