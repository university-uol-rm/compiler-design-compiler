package vm.generator;

/**
 * A halper class storing the semantic analysis state for program flow
 */
public class StaticSubroutineState {
	private boolean terminated;

	public StaticSubroutineState(boolean terminated) {
		this.terminated = terminated;
	}

	public StaticSubroutineState() {
		this(false);
	}

	public static StaticSubroutineState leastGuarantee(StaticSubroutineState ifState, StaticSubroutineState elseState) {
		StaticSubroutineState state = new StaticSubroutineState();

		if (ifState.hasTerminated() && elseState.hasTerminated())
			state.markTerminated();

		return state;
	}

	/**
	 * Mark the current branch as terminated
	 */
	public void markTerminated() {
		this.terminated = true;
	}

	/**
	 * @return true iff the branch has terminated
	 */
	public boolean hasTerminated() {
		return terminated;
	}
}
