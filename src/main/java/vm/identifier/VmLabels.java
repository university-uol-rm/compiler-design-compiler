package vm.identifier;

import syntactic.data.symbol.Subroutine;

/**
 * * Generates labels for VM labels
 */
public class VmLabels {
	private final Subroutine subroutine;
	private final int index;

	public VmLabels(Subroutine subroutine, int index) {
		this.subroutine = subroutine;
		this.index = index;
	}

	public Subroutine getSubroutine() {
		return subroutine;
	}

	public int getIndex() {
		return index;
	}
}
