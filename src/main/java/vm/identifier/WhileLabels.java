package vm.identifier;

import syntactic.data.symbol.Subroutine;

/**
 * * Generates labels for VM while labels
 */
public class WhileLabels extends VmLabels {
	public WhileLabels(Subroutine subroutine, int index) {
		super(subroutine, index);
	}

	public String getStartLabel() {
		return String.format("%s_while_%d_start", getSubroutine().getName(), getIndex());
	}

	public String getEndLabel() {
		return String.format("%s_while_%d_end", getSubroutine().getName(), getIndex());
	}
}
