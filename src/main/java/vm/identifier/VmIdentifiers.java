package vm.identifier;

import syntactic.data.JackClass;
import syntactic.data.symbol.Subroutine;

/**
 * Generates identifiers for VM subroutines
 */
public class VmIdentifiers {
	public static String identify(JackClass jackClass, Subroutine subroutine) {
		return String.format("%s.%s", jackClass.getName(), subroutine.getName());
	}
}
