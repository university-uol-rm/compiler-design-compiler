package vm.identifier;

import syntactic.data.symbol.Subroutine;

/**
 * Generates labels for VM if labels
 */
public class IfLabels extends VmLabels {
	public IfLabels(Subroutine subroutine, int index) {
		super(subroutine, index);
	}

	public String getBodyLabel() {
		return String.format("%s_if_%d_body", getSubroutine().getName(), getIndex());
	}

	public String getEndLabel() {
		return String.format("%s_if_%d_end", getSubroutine().getName(), getIndex());
	}
}
