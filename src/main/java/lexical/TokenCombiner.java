package lexical;

import exception.LexicalException;
import lexical.data.Token;
import lexical.data.TokenType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Combines multiple token sources into one source
 */
public class TokenCombiner implements TokenSource {
	final List<TokenSource> sources = new ArrayList<>();
	int current = 0;

	public void addSource(TokenSource source) {
		sources.add(source);
	}

	@Override
	public Token getNextToken() throws IOException, LexicalException {
		selectSource();
		if (isExhausted())
			return new Token(TokenType.EOF, "");
		return currentSource().getNextToken();
	}

	private boolean isExhausted() {
		return current >= sources.size();
	}

	private void selectSource() throws IOException, LexicalException {
		while (!isExhausted() && currentSource().peekNextToken().getType() == TokenType.EOF) current++;
	}

	private TokenSource currentSource() {
		return sources.get(current);
	}

	@Override
	public Token peekNextToken() throws IOException, LexicalException {
		selectSource();
		if (isExhausted())
			return new Token(TokenType.EOF, "");
		return currentSource().peekNextToken();
	}

	@Override
	public boolean hasNextToken() throws IOException, LexicalException {
		return !isExhausted() && currentSource().hasNextToken();
	}
}
