/**
 * Contains logic related to the lexical parsing of a Jack program. I.e. for splitting an input text into Jack tokens.
 */
package lexical;