package lexical;

import exception.LexicalException;
import lexical.data.Token;

import java.io.IOException;

/**
 * Represents the source of a token stream
 */
public interface TokenSource {
	/**
	 * Consumes and returns the next token
	 *
	 * @return the token
	 * @throws IOException      if an IO operation was unsuccessful
	 * @throws LexicalException if there is a lexical error in the source
	 */
	Token getNextToken() throws IOException, LexicalException;

	/**
	 * Returns the next token without removing it from the list
	 *
	 * @return the token
	 * @throws IOException      if an IO operation was unsuccessful
	 * @throws LexicalException if there is a lexical error in the source
	 */
	Token peekNextToken() throws IOException, LexicalException;

	/**
	 * Returns true if and only if there are more tokens. This returns true even if the last token, EOF, is still available
	 *
	 * @return
	 * @throws IOException
	 * @throws LexicalException
	 */
	boolean hasNextToken() throws IOException, LexicalException;
}
