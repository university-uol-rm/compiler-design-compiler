package lexical.data;

public class Token {
	private final TokenType type;
	private final String lexeme;
	private final int line, column;
	private final boolean locationKnown;
	private final String file;

	public Token(TokenType type, String lexeme) {
		this.type = type;
		this.lexeme = lexeme;
		this.file = null;
		line = column = 0;
		locationKnown = false;
	}

	public Token(TokenType type, String lexeme, int line, int column, String file) {
		this.type = type;
		this.lexeme = lexeme;
		this.line = line;
		this.column = column;
		this.file = file;
		locationKnown = true;
	}

	public int getLine() {
		if (!locationKnown) throw new RuntimeException("The location of this token is not known");
		return line;
	}

	public int getColumn() {
		if (!locationKnown) throw new RuntimeException("The location of this token is not known");
		return column;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Token token = (Token) o;
		return type == token.type &&
				lexeme.equals(token.lexeme);
	}

	public TokenType getType() {
		return type;
	}

	public String getLexeme() {
		return lexeme;
	}

	@Override
	public String toString() {
		return "<" +
				"\"" + lexeme + "\", " +
				type.toString() + ", " +
				"(" + line + ", " + column + ")" +
				">";
	}

	public String getFile() {
		return this.file;
	}
}
