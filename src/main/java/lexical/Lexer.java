package lexical;

import exception.LexicalException;
import exception.NoMoreTokensException;
import jack.Keyword;
import jack.Symbol;
import lexical.data.Token;
import lexical.data.TokenType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

/**
 * Responsible for lexical analysis
 */
public class Lexer implements TokenSource {
	private final BufferedReader input;
	private final String file;
	private Token calculatedNext = null;
	private boolean hasStarted = false, hasEnded = false;
	private String lineText = null;
	private int line = 0, column = 0;

	public Lexer(Reader input, String file) {
		this.input = new BufferedReader(input);
		this.file = file;
	}

	private static boolean isWordStart(char c) {
		return Character.isLetter(c) || c == '_';
	}

	private static boolean isWordCharacter(char c) {
		return Character.isLetterOrDigit(c) || c == '_';
	}

	private Token fetchNextToken() throws IOException, LexicalException {
		if (calculatedNext != null) return calculatedNext;

		if (hasEnded) throw new NoMoreTokensException("No more tokens", line, column, file);

		if (!hasStarted) nextLine();
		hasStarted = true;

		consumeEmpty();

		if (lineText == null || column >= lineText.length()) {
			hasEnded = true;
			return calculatedNext = new Token(TokenType.EOF, "", line, column, file);
		}

		return calculatedNext = consumeToken();
	}

	private Token consumeToken() throws LexicalException {
		char first = lineText.charAt(column);
		String firstAsString = Character.toString(first);

		if (first == '"')
			return consumeString();
		else if (Character.isDigit(first))
			return consumeInteger();
		else if (Symbol.getSymbols().contains(firstAsString))
			return consumeSymbol();
		else if (isWordStart(first))
			return consumeWord();
		else
			throw new LexicalException("Illegal character for token: " + first, line, column, file);
	}

	private Token consumeSymbol() {
		Token token = new Token(TokenType.SYMBOL, lineText.substring(column, column + 1), line, column, file);
		column++;
		return token;
	}

	private Token consumeWord() {
		int endIndex = column;

		while (endIndex < lineText.length()) {
			char endCharacter = lineText.charAt(endIndex);
			if (!isWordCharacter(endCharacter)) break;
			endIndex++;
		}

		String word = lineText.substring(column, endIndex);
		Token token;

		if (Keyword.allKeywords().contains(word))
			token = new Token(TokenType.KEYWORD, word, line, column, file);
		else if (Keyword.booleanValues().contains(word))
			token = new Token(TokenType.LITERAL_BOOLEAN, word, line, column, file);
		else if (word.equals(Keyword.NULL))
			token = new Token(TokenType.LITERAL_NULL, word, line, column, file);
		else
			token = new Token(TokenType.IDENTIFIER, word, line, column, file);

		column = endIndex;
		return token;
	}

	private Token consumeInteger() throws LexicalException {
		int endIndex = column;

		while (endIndex < lineText.length()) {
			char endCharacter = lineText.charAt(endIndex);

			if (Character.isAlphabetic(endCharacter))
				throw new LexicalException("Illegal character while parsing integer literal: " + endCharacter, line, endIndex, file);
			if (!Character.isDigit(endCharacter)) break;
			endIndex++;
		}

		String value = lineText.substring(column, endIndex);
		Token token = new Token(TokenType.LITERAL_INTEGER, value, line, column, file);

		column = endIndex;
		return token;
	}

	private Token consumeString() throws LexicalException {
		int endIndex = lineText.indexOf("\"", column + 1);

		if (endIndex < 0) {
			throw new LexicalException("Expected end of String before end of line but none found", line, column, file);
		} else {
			Token token = new Token(TokenType.LITERAL_STRING, lineText.substring(column, endIndex + 1), line, column, file);
			column = endIndex + 1;
			return token;
		}
	}

	private void consumeEmpty() throws IOException, LexicalException {
		while (true) {
			if (lineText == null) break;

			if (lineText.length() == column)
				nextLine();
			else if (lineText.substring(column, column + 1).matches("\\s"))
				column++;
			else if (matches("//"))
				nextLine();
			else if (matches("/*")) {
				column += 2;
				consumeMultiline();
			} else break;
		}
	}

	private void consumeMultiline() throws IOException, LexicalException {
		while (true) {
			int end = lineText.indexOf("*/", column);
			if (end >= 0) {
				column = end + 2;
				break;
			} else {
				nextLine();
				if (lineText == null)
					throw new LexicalException("Expected multi-line comment to end; found end of stream", line, column, file);
			}
		}

	}

	private void nextLine() throws IOException {
		lineText = input.readLine();
		line++;
		column = 0;
	}

	private boolean matches(String test) {
		if (lineText == null) return false;
		return lineText.regionMatches(column, test, 0, test.length());
	}

	public Token getNextToken() throws IOException, LexicalException {
		Token nextToken = fetchNextToken();
		calculatedNext = null;
		return nextToken;
	}

	public Token peekNextToken() throws IOException, LexicalException {
		return fetchNextToken();
	}

	public boolean hasNextToken() throws IOException, LexicalException {
		try {
			fetchNextToken();
		} catch (NoMoreTokensException ignored) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "lexical.Lexer{" +
				"token=" + (calculatedNext == null ? "null" : calculatedNext.toString()) +
				", line=" + line +
				", index=" + column +
				", content=" + (lineText == null ? "null" : "\"" + lineText.substring(column) + "\"") +
				'}';
	}
}
