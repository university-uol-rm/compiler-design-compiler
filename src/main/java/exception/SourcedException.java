package exception;

import lexical.data.Token;

public class SourcedException extends CompileException {
	private final Token token;

	public SourcedException(String message, Token token) {
		super(
				message,
				token.getLine(),
				token.getColumn(),
				token.getFile()
		);
		this.token = token;
	}

	@Override
	public String getHelpfulMessage() {
		return String.format(
				"%s - near token \"%s\" on line %d, column %d in file %s",
				token.getLexeme(),
				getMessage(),
				getLine(),
				getColumn(),
				getFile()
		);
	}

	public Token getToken() {
		return token;
	}
}
