package exception;

public class CompileException extends Exception {
	private final int line, column;
	private final String file;

	public CompileException(String message, int line, int column, String file) {
		super(message);
		this.line = line;
		this.column = column;
		this.file = file;
	}

	public int getLine() {
		return line;
	}

	public int getColumn() {
		return column;
	}

	public String getHelpfulMessage() {
		return String.format(
				"%s - on line %d, column %d in file %s",
				getMessage(),
				getLine(),
				getColumn(),
				getFile()
		);
	}

	public String getFile() {
		return file;
	}
}
