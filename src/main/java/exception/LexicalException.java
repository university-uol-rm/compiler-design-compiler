package exception;

public class LexicalException extends CompileException {
	public LexicalException(String message, int line, int column, String file) {
		super(message, line, column, file);
	}
}
