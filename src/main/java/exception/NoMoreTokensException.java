package exception;

public class NoMoreTokensException extends LexicalException {
	public NoMoreTokensException(String message, int line, int column, String file) {
		super(message, line, column, file);
	}
}
