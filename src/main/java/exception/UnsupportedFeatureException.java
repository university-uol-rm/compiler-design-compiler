package exception;

public class UnsupportedFeatureException extends RuntimeException {
	public UnsupportedFeatureException(String message) {
		super(message);
	}
}
