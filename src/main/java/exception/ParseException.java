package exception;

import lexical.data.Token;

public class ParseException extends SourcedException {
	public ParseException(String message, Token token) {
		super(message, token);
	}
}
