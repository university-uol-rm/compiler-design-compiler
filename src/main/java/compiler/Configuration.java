package compiler;

import output.Output;
import semantic.WarningReporter;

import java.io.File;

/**
 * Represents the compiler configuration
 */
public class Configuration {
	private String sourceDirectory = null;
	private WarningReporter warningReporter;
	private Output output;

	public WarningReporter getWarningReporter() {
		return warningReporter;
	}

	public void setWarningReporter(WarningReporter warningReporter) {
		this.warningReporter = warningReporter;
	}

	public File getSourceDirectory() {
		return new File(sourceDirectory);
	}

	public void setSourceDirectory(String sourceDirectory) {
		this.sourceDirectory = sourceDirectory;
	}

	public Output getOutput() {
		return output;
	}

	public void setOutput(Output output) {
		this.output = output;
	}
}
