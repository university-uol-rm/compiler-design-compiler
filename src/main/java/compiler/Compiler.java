package compiler;

import exception.CompileException;
import exception.InvalidInputException;
import lexical.Lexer;
import lexical.TokenCombiner;
import lexical.TokenSource;
import output.Output;
import syntactic.Parser;
import syntactic.data.Program;
import vm.generator.CodeGenerator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Represents the compiler
 */
public class Compiler {
	private final Configuration configuration;

	public Compiler(Configuration configuration) {
		this.configuration = configuration;
	}

	public TokenSource getLexer() throws InvalidInputException {
		TokenCombiner combiner = new TokenCombiner();

		File[] files = configuration.getSourceDirectory().listFiles();

		if (files == null)
			throw new InvalidInputException("The specified source must be a directory");

		for (File source : files) {
			if (!source.getName().endsWith(".jack"))
				continue;

			FileReader reader;
			try {
				reader = new FileReader(source);
			} catch (FileNotFoundException e) {
				throw new IllegalStateException("Existing file not found");
			}

			Lexer lexer = new Lexer(reader, source.getAbsolutePath());
			combiner.addSource(lexer);
		}

		return combiner;
	}

	public Parser getParser() throws InvalidInputException {
		return new Parser(getLexer());
	}

	public CodeGenerator getCodeGenerator() throws IOException, CompileException, InvalidInputException {
		Program program = new Program();
		getParser().parseProgram(program);
		return new CodeGenerator(program, configuration.getWarningReporter());
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void runCompilation() throws CompileException, InvalidInputException, IOException {
		CodeGenerator generator = getCodeGenerator();
		generator.preprocess();

		Output output = configuration.getOutput();
		output.output(generator);
	}
}
