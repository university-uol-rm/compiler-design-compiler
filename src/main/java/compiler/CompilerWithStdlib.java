package compiler;

import exception.CompileException;
import exception.InvalidInputException;
import semantic.ReportType;
import semantic.WarningReporter;
import syntactic.data.JackClass;
import syntactic.data.Program;
import vm.generator.CodeGenerator;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

/**
 * Extends the compiler to add the standard Jack OS library
 */
public class CompilerWithStdlib extends Compiler {
	public CompilerWithStdlib(Configuration configuration) {
		super(configuration);
	}

	protected Program generateProgramTemplate() throws InvalidInputException, IOException, CompileException {
		Configuration templateConfiguration = new Configuration();
		templateConfiguration.setSourceDirectory(getStdlibDirectory().getPath());
		templateConfiguration.setWarningReporter(new WarningReporter(ReportType.INTERRUPT));

		Compiler templateCompiler = new Compiler(templateConfiguration);

		Program program = new Program();
		templateCompiler.getParser().parseProgram(program);

		for (JackClass jackClass : program.getClasses()) {
			jackClass.setPrecompiled(getPrecompiledFile(jackClass));
		}
		return program;
	}

	private File getStdlibDirectory() {
		URL resource = CompilerWithStdlib.class.getResource("/os-jack");

		try {
			return Paths.get(resource.toURI()).toFile();
		} catch (URISyntaxException e) {
			throw new RuntimeException("Unexpected error: failed to get standard library directory", e);
		}
	}

	private File getPrecompiledFile(JackClass jackClass) {
		String location = String.format("/os-vm/%s.vm", jackClass.getName());
		URL resource = CompilerWithStdlib.class.getResource(location);

		try {
			return Paths.get(resource.toURI()).toFile();
		} catch (URISyntaxException e) {
			throw new RuntimeException("Unexpected error: failed to get precompiled class", e);
		}
	}

	public CodeGenerator getCodeGenerator() throws IOException, CompileException, InvalidInputException {
		Program program = generateProgramTemplate();
		getParser().parseProgram(program);
		return new CodeGenerator(program, getConfiguration().getWarningReporter());
	}
}
