package semantic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class that handles reporting non-critical semantic exceptions.
 */
public class WarningReporter {
	private final List<SemanticException> warnings = new ArrayList<>(0);
	private final ReportType reportType;

	public WarningReporter(ReportType reportType) {
		this.reportType = reportType;
	}

	public List<SemanticException> getWarnings() {
		return Collections.unmodifiableList(warnings);
	}

	public void handle(SemanticException exception) throws SemanticException {
		switch (reportType) {
			case WARN:
				warnings.add(exception);
				break;
			case INTERRUPT:
				throw exception;
		}
	}

	public ReportType getReportType() {
		return reportType;
	}
}