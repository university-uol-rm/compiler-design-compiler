package semantic;

import exception.SourcedException;
import lexical.data.Token;
import syntactic.data.Node;

/**
 * Represents a semantic error in the Jack program
 */
public class SemanticException extends SourcedException {
	public SemanticException(String message, Token token) {
		super(message, token);
	}

	public SemanticException(String message, Node node) {
		super(message, node.getSource());
	}
}
