package output;

import semantic.SemanticException;
import vm.generator.CodeGenerator;

import java.io.IOException;

/**
 * Handles compilation output
 */
public interface Output {
	void output(CodeGenerator codeGenerator) throws SemanticException, IOException;
}
