package output;

import semantic.SemanticException;
import syntactic.data.JackClass;
import vm.generator.CodeGenerator;

import java.io.IOException;

/**
 * Ignores all output
 * (Useful for some automated tests)
 */
public class NullOutput implements Output {
	@Override
	public void output(CodeGenerator codeGenerator) throws SemanticException, IOException {
		for (JackClass jackClass : codeGenerator.getProgram().getClasses()) {
			codeGenerator.compileClass(jackClass, (instruction, comment) -> {
			});
		}
	}
}
