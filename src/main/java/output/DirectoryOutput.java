package output;

import semantic.SemanticException;
import syntactic.data.JackClass;
import vm.generator.CodeGenerator;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * Directs the output of a {@link vm.generator.CodeGenerator} to a directory, creating that directory if necessary
 */
public class DirectoryOutput implements Output {
	private final File outputDirectory;

	public DirectoryOutput(File outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	@Override
	public void output(CodeGenerator codeGenerator) throws SemanticException, IOException {
		if (!outputDirectory.exists()) {
			if (!outputDirectory.mkdirs())
				throw new IOException("Could not create target directory");
		}

		for (JackClass jackClass : codeGenerator.getProgram().getClasses()) {
			File outputFile = getClassOutputFile(jackClass);
			PrintWriter writer = new PrintWriter(new PrintStream(outputFile));

			codeGenerator.compileClass(jackClass, (instruction, comment) -> {
				String line = instruction;

				if (comment != null)
					line += String.format("\t// %s", comment);

				writer.println(line);
			});

			writer.flush();
			writer.close();
		}
	}

	private File getClassOutputFile(JackClass jackClass) {
		String fileName = String.format("%s/%s.vm", outputDirectory.getPath(), jackClass.getName());
		return new File(fileName);
	}
}
