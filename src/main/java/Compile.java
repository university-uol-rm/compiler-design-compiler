import cli.CommandLineCompiler;

public class Compile {
	public static void main(String[] args) {
		CommandLineCompiler compiler = new CommandLineCompiler();
		compiler.process(args);
	}
}
