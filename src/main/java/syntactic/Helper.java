package syntactic;

public class Helper {
	public static boolean oneOf(String word, String[] words) {
		for (String option : words)
			if (word.equals(option)) return true;

		return false;
	}
}
