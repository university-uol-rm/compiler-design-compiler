package syntactic.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Program extends Node {
	private final Map<String, JackClass> classes = new HashMap<>();

	public Program() {
		super(null);
	}

	public boolean containsJackClass(String name) {
		return classes.containsKey(name);
	}

	public JackClass getJackClass(String name) {
		return classes.get(name);
	}

	public Collection<JackClass> getClasses() {
		return classes.values();
	}

	public void putClass(String className, JackClass jackClass) {
		classes.put(className, jackClass);
	}
}
