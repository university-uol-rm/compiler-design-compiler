package syntactic.data;

import lexical.data.Token;
import semantic.SemanticException;
import syntactic.data.symbol.Subroutine;
import syntactic.data.symbol.Variable;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class JackClass extends Node {
	private final Map<String, Subroutine> subroutines = new HashMap<>();
	private final VariableTable fields = new VariableTable();
	private final String name;
	private File precompiled = null;

	public JackClass(Token source, String name) {
		super(source);
		this.name = name;
	}

	public File getPrecompiled() {
		if (precompiled == null)
			throw new RuntimeException("No precompiled file was provided");

		return precompiled;
	}

	public boolean isPrecompiled() {
		return precompiled != null;
	}

	public void setPrecompiled(File precompiled) {
		this.precompiled = precompiled;
	}

	public Variable getField(String key) {
		return fields.get(key);
	}

	public boolean containsField(String key) {
		return fields.containsKey(key);
	}

	public void putField(String identifier, Variable field) throws SemanticException {
		fields.register(identifier, field);
	}

	public Subroutine getSubroutine(String key) {
		return subroutines.get(key);
	}

	public boolean containsSubroutine(String key) {
		return subroutines.containsKey(key);
	}

	public void putSubroutine(String identifier, Subroutine subroutine) throws SemanticException {
		if (subroutines.containsKey(identifier)) {
			throw new SemanticException(
					"A subroutine named " + identifier + " has already been defined in this class.",
					subroutine
			);
		}
		subroutines.put(identifier, subroutine);
	}

	public String getName() {
		return name;
	}

	public Collection<Variable> getFields() {
		return fields.values();
	}

	public Collection<Subroutine> getSubroutines() {
		return subroutines.values();
	}

	/**
	 * Returns the memory size of an instance of this Jack class, in bytes
	 */
	public int getSize() {
		return fields.size();
	}
}
