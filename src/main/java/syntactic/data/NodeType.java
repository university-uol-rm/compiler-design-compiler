package syntactic.data;

public enum NodeType {
	NODE_GROUP,
	OTHER,

	PROGRAM,

	CLASS_DECLARATION,
	VARIABLE_DECLARATION,
	SUBROUTINE_DECLARATION,

	PARAMETER_LIST,
	PARAMETER,

	STATEMENT_BLOCK,

	STATEMENT_RETURN,
	STATEMENT_LET,
	STATEMENT_IF,
	STATEMENT_WHILE,

	SUBROUTINE_CALL,
	EXPRESSION_LIST,

	EXPRESSION,
	EXPRESSION_RELATIONAL,
	EXPRESSION_ARITHMETIC,
	EXPRESSION_TERM,
	EXPRESSION_FACTOR,
	EXPRESSION_LITERAL,
	EXPRESSION_THIS,

	TYPE,
	IDENTIFIER,
	IDENTIFIER_PATH,
	ARRAY_PATH;

	public String abbreviation() {
		switch (this) {
			case NODE_GROUP:
				return "";
			case PROGRAM:
				return "prog";
			case CLASS_DECLARATION:
				return "class";
			case VARIABLE_DECLARATION:
				return "var";
			case SUBROUTINE_DECLARATION:
				return "fn";
			case PARAMETER_LIST:
				return "params";
			case PARAMETER:
				return "param";
			case STATEMENT_BLOCK:
				return "block";
			case STATEMENT_RETURN:
				return "return";
			case STATEMENT_LET:
				return "=";
			case STATEMENT_IF:
				return "if";
			case STATEMENT_WHILE:
				return "while";
			case SUBROUTINE_CALL:
				return "call";
			case EXPRESSION_LIST:
				return "exp.lst";
			case EXPRESSION:
				return "exp";
			case EXPRESSION_RELATIONAL:
				return "exp.rel";
			case EXPRESSION_ARITHMETIC:
				return "exp.arithm";
			case EXPRESSION_TERM:
				return "exp.term";
			case EXPRESSION_FACTOR:
				return "exp.fctr";
			case EXPRESSION_LITERAL:
				return "exp.op";
			case TYPE:
				return "type";
			case IDENTIFIER:
				return "id";
			case IDENTIFIER_PATH:
				return "id.id";
			case ARRAY_PATH:
				return "[]";
		}
		return this.toString();
	}
}
