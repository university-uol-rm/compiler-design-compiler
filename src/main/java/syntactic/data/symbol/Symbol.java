package syntactic.data.symbol;

import lexical.data.Token;
import syntactic.data.Node;
import vm.type.Type;

public class Symbol extends Node {
	private final Type type;
	private final boolean isStatic;
	private final String name;

	public Symbol(Token source, Type type, boolean isStatic, String name) {
		super(source);
		this.type = type;
		this.isStatic = isStatic;
		this.name = name;
	}

	public Type getType() {
		return type;
	}

	public boolean isStatic() {
		return isStatic;
	}

	public String getName() {
		return name;
	}
}