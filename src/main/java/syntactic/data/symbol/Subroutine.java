package syntactic.data.symbol;

import lexical.data.Token;
import semantic.SemanticException;
import syntactic.data.CodeNode;
import syntactic.data.VariableTable;
import vm.type.Type;

import java.util.Collections;
import java.util.List;

public class Subroutine extends Symbol {
	private final List<Parameter> orderedParameters;
	private final VariableTable parameters = new VariableTable();
	private final boolean isConstructor;
	private final CodeNode body;
	private String vmIdentifier;

	public Subroutine(
			Token source,
			Type type,
			boolean isStatic,
			boolean isConstructor,
			CodeNode body,
			String name,
			List<Parameter> orderedParameters
	) throws SemanticException {
		super(source, type, isStatic, name);
		this.isConstructor = isConstructor;
		this.body = body;
		this.orderedParameters = orderedParameters;

		for (Parameter parameter : orderedParameters) {
			parameters.register(parameter.getName(), parameter);
		}
	}

	public VariableTable getParameters() {
		return parameters;
	}

	public List<Parameter> getOrderedParameters() {
		return Collections.unmodifiableList(orderedParameters);
	}

	public CodeNode getBody() {
		return body;
	}

	@Override
	public boolean isStatic() {
		return super.isStatic() || isConstructor;
	}

	@Override
	public String toString() {
		return "Subroutine: " +
				(isStatic() ? "static " : "") +
				(isConstructor ? "constructor" : (getType().getName() + " " + getName()));
	}

	public boolean isConstructor() {
		return isConstructor;
	}

	public String getVmIdentifier() {
		return vmIdentifier;
	}

	public void setVmIdentifier(String vmIdentifier) {
		this.vmIdentifier = vmIdentifier;
	}
}
