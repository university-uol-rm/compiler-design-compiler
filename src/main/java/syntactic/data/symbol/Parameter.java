package syntactic.data.symbol;

import lexical.data.Token;
import vm.type.Type;

public class Parameter extends Variable {
	public Parameter(Token source, Type type, String name) {
		super(source, type, false, name);
		markInitialized();
	}
}
