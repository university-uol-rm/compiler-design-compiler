package syntactic.data.symbol;

import lexical.data.Token;
import vm.type.Type;

public class Field extends Variable {
	public Field(Token source, Type type, boolean isStatic, String name) {
		super(source, type, isStatic, name);
		markInitialized();
	}
}
