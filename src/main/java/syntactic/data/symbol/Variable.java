package syntactic.data.symbol;

import lexical.data.Token;
import vm.type.Type;

public class Variable extends Symbol {
	private int index;
	private boolean inScope;
	private boolean isInitialized = false;

	public Variable(Token source, Type type, boolean isStatic, String name) {
		super(source, type, isStatic, name);
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	@Override
	public String toString() {
		return "Field: " +
				(isStatic() ? "static " : "") +
				getNodeType() + " " +
				getName();
	}

	public int getSize() {
		return 1;
	}

	public boolean isInScope() {
		return inScope;
	}

	public void setInScope(boolean inScope) {
		this.inScope = inScope;
	}

	public void markInitialized() {
		isInitialized = true;
	}

	public boolean isInitialized() {
		return isInitialized;
	}
}
