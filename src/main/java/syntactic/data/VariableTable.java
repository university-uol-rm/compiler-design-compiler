package syntactic.data;

import semantic.SemanticException;
import syntactic.data.symbol.Variable;

import java.util.HashMap;

public class VariableTable extends HashMap<String, Variable> {
	/**
	 * Utility method that registers a variable if it has not been registered and throws a semantic exception otherwise
	 *
	 * @param name  the name of the variable
	 * @param field the variable to register
	 * @throws SemanticException if a variable of this name has already been registered
	 */
	public void register(String name, Variable field) throws SemanticException {
		if (containsKey(name)) {
			throw new SemanticException(
					String.format("A field named %s has already been defined.", name),
					field
			);
		}
		super.put(name, field);
	}
}
