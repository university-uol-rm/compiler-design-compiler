package syntactic.data;

import lexical.data.Token;

public class Node {
	private final Token source;

	public Node(Token source) {
		this.source = source;
	}

	public Token getSource() {
		return source;
	}

	public NodeType getNodeType() {
		return NodeType.OTHER;
	}
}
