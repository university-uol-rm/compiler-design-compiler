package syntactic.data;

import lexical.data.Token;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CodeNode extends Node {
	private final NodeType type;
	private final List<Node> children = new ArrayList<>(2);

	public CodeNode(NodeType type, Token source) {
		super(source);
		this.type = type;
	}

	public NodeType getNodeType() {
		return type;
	}

	public int childCount() {
		return children.size();
	}

	public void addChild(Node node) {
		children.add(node);
	}

	public Node getChild(int index) {
		return children.get(index);
	}

	public List<Node> getChildren() {
		return Collections.unmodifiableList(children);
	}
}
