package syntactic;

import exception.LexicalException;
import exception.ParseException;
import jack.Keyword;
import lexical.TokenSource;
import lexical.data.Token;
import lexical.data.TokenType;
import syntactic.data.CodeNode;
import syntactic.data.NodeType;

import java.io.IOException;

import static syntactic.Helper.oneOf;

interface SubParser {
	CodeNode parse() throws IOException, LexicalException, ParseException;
}

public class ExpressionParser {
	private final static String[] booleanOperators = new String[]{"&", "|"};
	private final static String[] relationalOperators = new String[]{"=", "<", ">"};
	private final static String[] arithmeticOperators = new String[]{"+", "-"};
	private final static String[] geometricOperators = new String[]{"*", "/"};
	private final static String[] unaryOperators = new String[]{"-", "~"};

	private final TokenSource tokens;

	public ExpressionParser(TokenSource tokens) {
		this.tokens = tokens;
	}

	public CodeNode parseExpression() throws IOException, LexicalException, ParseException {
		return doParse(
				this::parseRelationalExpression,
				booleanOperators,
				NodeType.EXPRESSION
		);
	}

	private CodeNode parseRelationalExpression() throws IOException, LexicalException, ParseException {
		return doParse(
				this::parseArithmeticExpression,
				relationalOperators,
				NodeType.EXPRESSION_RELATIONAL
		);
	}

	private CodeNode parseArithmeticExpression() throws IOException, LexicalException, ParseException {
		return doParse(
				this::parseTerm,
				arithmeticOperators,
				NodeType.EXPRESSION_ARITHMETIC
		);
	}

	private CodeNode parseTerm() throws IOException, LexicalException, ParseException {
		return doParse(
				this::parseFactor,
				geometricOperators,
				NodeType.EXPRESSION_TERM
		);
	}

	private CodeNode doParse(SubParser parser, String[] validOperators, NodeType type) throws IOException, LexicalException, ParseException {
		CodeNode term = parser.parse();

		while (true) {
			Token operator = tokens.peekNextToken();
			if (!oneOf(operator.getLexeme(), validOperators)) break;

			tokens.getNextToken();

			CodeNode rightTerm = parser.parse();

			CodeNode newExpression = new CodeNode(type, operator);
			newExpression.addChild(term);
			newExpression.addChild(rightTerm);
			term = newExpression;
		}

		return term;
	}

	private CodeNode parseFactor() throws IOException, LexicalException, ParseException {
		Token operator = tokens.peekNextToken();

		if (oneOf(operator.getLexeme(), unaryOperators)) {
			CodeNode factor = new CodeNode(NodeType.EXPRESSION_FACTOR, operator);
			tokens.getNextToken();

			factor.addChild(parseOperand());
			return factor;
		}

		return parseOperand();
	}

	private CodeNode parseOperand() throws IOException, LexicalException, ParseException {
		Token first = tokens.peekNextToken();
		String lexeme = first.getLexeme();

		switch (first.getType()) {
			case LITERAL_BOOLEAN:
			case LITERAL_INTEGER:
			case LITERAL_STRING:
			case LITERAL_NULL:
				tokens.getNextToken();
				return new CodeNode(NodeType.EXPRESSION_LITERAL, first);

			case KEYWORD:
				if (lexeme.equals(Keyword.THIS)) {
					tokens.getNextToken();
					return new CodeNode(NodeType.EXPRESSION_THIS, first);
				}
				break;

			case SYMBOL:
				if (first.getLexeme().equals("("))
					return parseParenthesisEnclosedExpression();
				break;

			case IDENTIFIER:
				CodeNode id = parseIdentifierPath();

				Token open = tokens.peekNextToken();
				if (open.getLexeme().equals("[")) {
					CodeNode access = new CodeNode(NodeType.ARRAY_PATH, open);
					access.addChild(id);
					access.addChild(parseArrayIndex());
					return access;
				}

				if (open.getLexeme().equals("(")) {
					CodeNode call = new CodeNode(NodeType.SUBROUTINE_CALL, open);
					call.addChild(id);
					call.addChild(parseExpressionList());
					return call;
				}
				return id;
		}

		throw new ParseException("While parsing expression, found illegal token", first);
	}

	public CodeNode parseParenthesisEnclosedExpression() throws LexicalException, ParseException, IOException {
		Token open = tokens.getNextToken();
		if (!open.getLexeme().equals("("))
			throw new ParseException("Expected enclosed expression to start with opening parenthesis, found illegal token", open);

		CodeNode expression = parseExpression();

		Token close = tokens.getNextToken();
		if (!close.getLexeme().equals(")"))
			throw new ParseException("Expected enclosed expression to end with closing parenthesis, found illegal token", close);

		return expression;
	}

	public CodeNode parseArrayIndex() throws LexicalException, ParseException, IOException {
		Token open = tokens.getNextToken();
		if (!open.getLexeme().equals("["))
			throw new ParseException("Expected array index expression to start with opening bracket \"[\", found illegal token", open);

		CodeNode expression = parseExpression();

		Token close = tokens.getNextToken();
		if (!close.getLexeme().equals("]"))
			throw new ParseException("Expected array index expression to end with closing bracket \"]\", found illegal token", close);

		return expression;
	}

	public CodeNode parseExpressionList() throws IOException, LexicalException, ParseException {
		Token open = tokens.getNextToken();

		if (!open.getLexeme().equals("("))
			throw new ParseException("Expected expression list to start with opening parenthesis \"(\", found illegal token", open);

		CodeNode list = new CodeNode(NodeType.EXPRESSION_LIST, open);

		boolean first = true;
		while (true) {
			Token next = tokens.peekNextToken();

			if (next.getLexeme().equals(")")) {
				tokens.getNextToken();
				break;
			}

			if (!first) {
				tokens.getNextToken();
				if (!next.getLexeme().equals(","))
					throw new ParseException("While parsing expression list, expected closing parenthesis or comma separator, found illegal token", next);
			}
			first = false;

			list.addChild(parseExpression());
		}

		return list;
	}

	public CodeNode parseIdentifierPath() throws IOException, LexicalException, ParseException {
		int level = 0;
		CodeNode currentID = parseIdentifier();

		while (true) {
			Token dot = tokens.peekNextToken();
			if (!dot.getLexeme().equals(".")) break;

			if (level > 0)
				throw new ParseException("Identifier paths can be at most one level deep, found additional dot after identifier path", dot);

			tokens.getNextToken();
			CodeNode path = new CodeNode(NodeType.IDENTIFIER_PATH, dot);

			CodeNode identifier;
			try {
				identifier = parseIdentifier();
			} catch (ParseException p) {
				throw new ParseException("While parsing identifier path, expected and identifier to follow dot, found illegal token", p.getToken());
			}

			path.addChild(currentID);
			path.addChild(identifier);
			currentID = path;

			level++;
		}

		return currentID;
	}

	private CodeNode parseIdentifier() throws ParseException, IOException, LexicalException {
		Token first = tokens.getNextToken();

		if (first.getType() != TokenType.IDENTIFIER)
			throw new ParseException("While parsing identifier, found illegal token", first);

		return new CodeNode(NodeType.IDENTIFIER, first);
	}
}