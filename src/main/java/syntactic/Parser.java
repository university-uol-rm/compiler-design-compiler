package syntactic;

import exception.CompileException;
import exception.LexicalException;
import exception.ParseException;
import jack.Keyword;
import lexical.TokenSource;
import lexical.data.Token;
import lexical.data.TokenType;
import semantic.SemanticException;
import syntactic.data.CodeNode;
import syntactic.data.JackClass;
import syntactic.data.NodeType;
import syntactic.data.Program;
import syntactic.data.symbol.Field;
import syntactic.data.symbol.Parameter;
import syntactic.data.symbol.Subroutine;
import syntactic.data.symbol.Variable;
import vm.type.Type;
import vm.type.VoidType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Parses the jack program
 */
public class Parser {
	private final TokenSource tokens;
	private final ExpressionParser expressionParser;

	public Parser(TokenSource tokens) {
		this.tokens = tokens;
		expressionParser = new ExpressionParser(tokens);
	}

	public void parseProgram(Program output) throws IOException, CompileException {
		while (tokens.hasNextToken()) {
			Token next = tokens.peekNextToken();
			if (next.getType() == TokenType.EOF) break;
			JackClass jackClass = parseClassDeclaration();

			String name = jackClass.getName();
			if (output.containsJackClass(name))
				throw new SemanticException(String.format("The class %s has already been declared", name), next);

			output.putClass(name, jackClass);
		}
	}

	private JackClass parseClassDeclaration() throws IOException, CompileException {
		Token keyword = tokens.getNextToken();
		if (!keyword.getLexeme().equals(Keyword.CLASS))
			throw new ParseException("Expected class declaration, found illegal token", keyword);

		Token name = tokens.getNextToken();
		if (name.getType() != TokenType.IDENTIFIER)
			throw new ParseException("Expected class name, found illegal token", keyword);

		JackClass classDeclaration = new JackClass(name, name.getLexeme());

		Token start = tokens.getNextToken();
		if (!start.getLexeme().equals("{"))
			throw new ParseException("Expected opening bracket '{', found illegal token", keyword);

		while (true) {
			Token next = tokens.peekNextToken();

			if (next.getType() == TokenType.EOF)
				throw new ParseException("Expected member declaration or end of class, found EOF", next);

			if (next.getLexeme().equals("}")) {
				tokens.getNextToken();
				break;
			}

			Token member = tokens.peekNextToken();

			if (Keyword.classVariableKeywords().contains(member.getLexeme())) {
				List<Variable> fields = parseFieldDeclaration();
				for (Variable field : fields)
					classDeclaration.putField(field.getName(), field);
			} else if (Keyword.functionKeywords().contains(member.getLexeme())) {
				Subroutine subroutine = parseSubroutineDeclaration();
				classDeclaration.putSubroutine(subroutine.getName(), subroutine);
			} else {
				throw new ParseException("Expected subroutine or field declaration, found illegal token", member);
			}
		}

		return classDeclaration;
	}

	private Subroutine parseSubroutineDeclaration() throws IOException, CompileException {
		Token keyword = tokens.getNextToken();
		String functionType = keyword.getLexeme();

		if (!Keyword.functionKeywords().contains(functionType))
			throw new ParseException("Expected a subroutine keyword, found illegal token", keyword);

		boolean isStatic = functionType.equals(Keyword.FUNCTION);
		boolean isConstructor = functionType.equals(Keyword.CONSTRUCTOR);

		Token returnTypeToken = tokens.peekNextToken();
		Type returnType;

		if (returnTypeToken.getLexeme().equals(Keyword.VOID)) {
			tokens.getNextToken();
			returnType = VoidType.VOID_TYPE;
		} else {
			try {
				returnType = new Type(parseType().getSource().getLexeme());
			} catch (ParseException p) {
				throw new ParseException("While parsing subroutine return type, expected \"void\" or primitive type, or identifier, found illegal token", p.getToken());
			}
		}

		Token name = tokens.getNextToken();
		if (name.getType() != TokenType.IDENTIFIER)
			throw new ParseException("While parsing subroutine name, expected identifier, found illegal token", name);

		List<Parameter> parameters = parseParameterList();
		CodeNode body = parseStatementBlock();

		return new Subroutine(
				keyword,
				returnType,
				isStatic,
				isConstructor,
				body,
				name.getLexeme(),
				parameters
		);
	}

	private CodeNode parseStatementBlock() throws IOException, CompileException {
		Token open = tokens.getNextToken();
		if (!open.getLexeme().equals("{"))
			throw new ParseException("While parsing statement block, expected opening brace, found illegal token", open);

		CodeNode statementBlock = new CodeNode(NodeType.STATEMENT_BLOCK, open);

		while (true) {
			Token next = tokens.peekNextToken();

			if (next.getLexeme().equals("}")) {
				tokens.getNextToken();
				break;
			}

			statementBlock.addChild(parseStatement());
		}

		return statementBlock;
	}

	private CodeNode parseStatement() throws IOException, CompileException {
		Token next = tokens.peekNextToken();

		switch (next.getLexeme()) {
			case Keyword.VAR:
				CodeNode group = new CodeNode(NodeType.NODE_GROUP, next);
				for (Variable node : parseVariableDeclaration()) {
					group.addChild(node);
				}
				return group;
			case Keyword.LET:
				return parseLetStatement();
			case Keyword.IF:
				return parseIfStatement();
			case Keyword.WHILE:
				return parseWhileStatement();
			case Keyword.DO:
				return parseDoStatement();
			case Keyword.RETURN:
				return parseReturnStatement();
			default:
				throw new ParseException("Expected statement, found illegal token", next);
		}
	}

	private CodeNode parseReturnStatement() throws IOException, LexicalException, ParseException {
		Token keyword = tokens.getNextToken();

		if (!keyword.getLexeme().equals(Keyword.RETURN))
			throw new ParseException("While parsing return statement, expected \"return\" keyword, found illegal token", keyword);

		CodeNode node = new CodeNode(NodeType.STATEMENT_RETURN, keyword);

		Token next = tokens.peekNextToken();
		if (next.getLexeme().equals(";")) {
			tokens.getNextToken();
			return node;
		}

		node.addChild(expressionParser.parseExpression());

		consumeSemicolon();

		return node;
	}

	private CodeNode parseDoStatement() throws ParseException, IOException, LexicalException {
		Token doToken = tokens.getNextToken();

		if (!doToken.getLexeme().equals(Keyword.DO))
			throw new ParseException("Expected do statement to start with \"do\" keyword, found illegal token", doToken);

		CodeNode identifier = expressionParser.parseIdentifierPath();

		Token open = tokens.peekNextToken();

		if (!open.getLexeme().equals("("))
			throw new ParseException("Expected opening parenthesis when parsing do statement; found illegal token", open);

		CodeNode call = new CodeNode(NodeType.SUBROUTINE_CALL, open);
		call.addChild(identifier);
		call.addChild(expressionParser.parseExpressionList());

		consumeSemicolon();

		return call;
	}

	private void consumeSemicolon() throws IOException, LexicalException, ParseException {
		Token semicolon = tokens.getNextToken();
		if (!semicolon.getLexeme().equals(";"))
			throw new ParseException("Expected statement to terminate with semicolon \";\", found illegal token", semicolon);
	}

	private CodeNode parseWhileStatement() throws IOException, CompileException {
		Token whileToken = tokens.getNextToken();

		if (!whileToken.getLexeme().equals(Keyword.WHILE))
			throw new ParseException("Expected while statement to start with \"while\" keyword, found illegal token", whileToken);

		CodeNode whileStatement = new CodeNode(NodeType.STATEMENT_WHILE, whileToken);
		whileStatement.addChild(expressionParser.parseParenthesisEnclosedExpression());
		whileStatement.addChild(parseStatementBlock());

		return whileStatement;
	}

	private CodeNode parseIfStatement() throws IOException, CompileException {
		Token ifToken = tokens.getNextToken();

		if (!ifToken.getLexeme().equals(Keyword.IF))
			throw new ParseException("Expected if statement to start with \"if\" keyword, found illegal token", ifToken);

		CodeNode ifStatement = new CodeNode(NodeType.STATEMENT_IF, ifToken);
		ifStatement.addChild(expressionParser.parseParenthesisEnclosedExpression());
		ifStatement.addChild(parseStatementBlock());

		Token elseToken = tokens.peekNextToken();

		if (elseToken.getLexeme().equals(Keyword.ELSE)) {
			tokens.getNextToken();
			ifStatement.addChild(parseStatementBlock());
		}

		return ifStatement;
	}

	private CodeNode parseLetStatement() throws IOException, LexicalException, ParseException {
		Token let = tokens.getNextToken();

		if (!let.getLexeme().equals(Keyword.LET))
			throw new ParseException("Expected let statement to start with \"let\" keyword, found illegal token", let);

		CodeNode letStatement = new CodeNode(NodeType.STATEMENT_LET, let);

		Token identifierToken = tokens.getNextToken();
		if (identifierToken.getType() != TokenType.IDENTIFIER)
			throw new ParseException("Expected an identifier in let statement, found illegal token", let);

		CodeNode identifier = new CodeNode(NodeType.IDENTIFIER, identifierToken);

		Token next = tokens.peekNextToken();
		if (next.getLexeme().equals("[")) {
			CodeNode index = expressionParser.parseArrayIndex();
			CodeNode arrayPath = new CodeNode(NodeType.ARRAY_PATH, next);
			arrayPath.addChild(identifier);
			arrayPath.addChild(index);
			identifier = arrayPath;
		}
		letStatement.addChild(identifier);

		Token equals = tokens.getNextToken();
		if (!equals.getLexeme().equals("="))
			throw new ParseException("Expected assignment operator \"=\" to follow identifier in let statement; found illegal token", equals);

		letStatement.addChild(expressionParser.parseExpression());

		consumeSemicolon();

		return letStatement;
	}

	private List<Parameter> parseParameterList() throws IOException, CompileException {
		List<Parameter> parameters = new ArrayList<>();

		Token open = tokens.getNextToken();

		if (!open.getLexeme().equals("("))
			throw new ParseException("While parsing parameter list, expected opening parenthesis \"(\", found illegal token", open);

		boolean firstParam = true;
		while (true) {
			Token next = tokens.peekNextToken();

			if (next.getLexeme().equals(")")) {
				tokens.getNextToken();
				break;
			}

			if (!firstParam) {
				Token comma = tokens.getNextToken();
				if (!comma.getLexeme().equals(","))
					throw new ParseException("While parsing parameter list, expected comma or closing parenthesis, found illegal token", comma);
			}

			parameters.add(parseParameter());

			firstParam = false;
		}

		return parameters;
	}

	private Parameter parseParameter() throws IOException, CompileException {
		CodeNode type = parseType();
		Token identifier = tokens.getNextToken();

		if (identifier.getType() != TokenType.IDENTIFIER)
			throw new ParseException("While parsing parameter, expected identifier, found illegal token", identifier);


		return new Parameter(
				type.getSource(),
				new Type(type.getSource().getLexeme()),
				identifier.getLexeme()
		);
	}

	private CodeNode parseType() throws IOException, CompileException {
		Token type = tokens.getNextToken();

		if (type.getType() == TokenType.IDENTIFIER)
			return new CodeNode(NodeType.TYPE, type);

		if (Keyword.primitiveTypes().contains(type.getLexeme()))
			return new CodeNode(NodeType.TYPE, type);

		throw new ParseException("While parsing type, expected primitive type keyword or identifier, found illegal token", type);
	}

	private List<Variable> parseVariableDeclaration() throws CompileException, IOException {
		List<Variable> variables = new ArrayList<>();

		Token keyword = tokens.getNextToken();

		if (!keyword.getLexeme().equals(Keyword.VAR))
			throw new ParseException("While parsing variable declaration, expected var keyword, found illegal token", keyword);

		Type type = new Type(parseType().getSource().getLexeme());

		while (true) {
			Token identifier = tokens.getNextToken();
			if (identifier.getType() != TokenType.IDENTIFIER)
				throw new ParseException("While parsing variable declaration, expected identifier, found illegal token", identifier);

			Variable variable = new Variable(keyword, type, false, identifier.getLexeme());
			variables.add(variable);

			Token next = tokens.getNextToken();

			if (next.getLexeme().equals(";")) break;

			if (!next.getLexeme().equals(","))
				throw new ParseException("While parsing variable declaration, expected separator (,) or terminator (;), found illegal token", next);
		}

		return variables;
	}

	private List<Variable> parseFieldDeclaration() throws CompileException, IOException {
		List<Variable> fields = new ArrayList<>(1);

		Token keyword = tokens.getNextToken();

		String keywordLexeme = keyword.getLexeme();
		if (!Keyword.classVariableKeywords().contains(keywordLexeme))
			throw new ParseException("While parsing field declaration, expected keyword, found illegal token", keyword);

		boolean isStatic = keywordLexeme.equals(Keyword.STATIC);

		CodeNode type = parseType();

		while (true) {
			Token identifier = tokens.getNextToken();
			if (identifier.getType() != TokenType.IDENTIFIER)
				throw new ParseException("While parsing field declaration, expected identifier, found illegal token", identifier);

			Field field = new Field(
					keyword,
					new Type(type.getSource().getLexeme()),
					isStatic,
					identifier.getLexeme()
			);
			fields.add(field);

			Token next = tokens.getNextToken();

			if (next.getLexeme().equals(";")) break;

			if (!next.getLexeme().equals(","))
				throw new ParseException("While parsing field declaration, expected separator (,) or terminator (;), found illegal token", next);
		}

		return fields;
	}
}
