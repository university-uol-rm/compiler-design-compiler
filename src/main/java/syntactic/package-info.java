/**
 * Contains logic for syntactic analysing and parsing.
 */
package syntactic;