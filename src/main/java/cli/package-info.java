/**
 * Contains an implementation of a command-line interface for the compiler
 * Parses the arguments and invokes the compiler
 */
package cli;