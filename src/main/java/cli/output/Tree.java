package cli.output;

import lexical.data.Token;
import syntactic.data.CodeNode;
import syntactic.data.JackClass;
import syntactic.data.Node;
import syntactic.data.Program;
import syntactic.data.symbol.Parameter;
import syntactic.data.symbol.Subroutine;
import syntactic.data.symbol.Variable;

import java.util.Collections;
import java.util.List;

public class Tree {
	public void showProgram(Program program) {
		for (JackClass jackClass : program.getClasses()) {
			System.out.println("class " + jackClass.getName());

			for (Variable field : jackClass.getFields()) {
				System.out.println("\t" + field.toString());
			}

			for (Subroutine subroutine : jackClass.getSubroutines()) {
				System.out.println("\t" + subroutine.toString() + parameterString(subroutine.getOrderedParameters()));
				showNode(subroutine.getBody(), 0);
			}
		}
	}

	private String parameterString(List<Parameter> parameters) {
		StringBuilder string = new StringBuilder("(");
		boolean first = true;
		for (Parameter parameter : parameters) {
			if (!first) string.append(", ");
			first = false;

			string
					.append(parameter.getNodeType())
					.append(" ")
					.append(parameter.getName());
		}
		return string + ")";
	}

	private void showNode(Node node, int level) {
		Token token = node.getSource();

		String pipes = "\t" + String.join("", Collections.nCopies(level, "   |"));

		System.out.println(
				pipes + "——" +
						node.getNodeType().abbreviation() +
						(token == null ? "" : ": " + token.getLexeme())
		);

		if (node instanceof CodeNode) {
			CodeNode codeNode = (CodeNode) node;
			for (int i = 0; i < codeNode.childCount(); i++) {
				showNode(codeNode.getChild(i), level + 1);
			}
		}
	}
}
