package cli.output;

import exception.LexicalException;
import lexical.TokenSource;

import java.io.IOException;

public class Token {
	public void listTokens(TokenSource lexer) throws IOException, LexicalException {
		while (lexer.hasNextToken())
			System.out.println(lexer.getNextToken());
	}
}
