package cli;

import cli.args.ArgumentException;
import cli.args.ArgumentParser;
import cli.output.Token;
import cli.output.Tree;
import compiler.Compiler;
import compiler.CompilerWithStdlib;
import compiler.Configuration;
import exception.CompileException;
import exception.InvalidInputException;
import semantic.ReportType;
import semantic.WarningReporter;

import java.io.IOException;

/**
 * Class responsible for handling the command-line interface
 */
public class CommandLineCompiler {
	Tree treeOutput = new Tree();
	Token tokenOutput = new Token();
	ArgumentParser argumentParser = new ArgumentParser();

	/**
	 * Show usage information to the user
	 */
	private static void showHelp() {
		System.out.println(
				"Usage:\n" +
						"Compile <input directory> <output directory> = Compiles the specified program to the specified directory." +
						"Compile <input directory> = Compiles the specified program in the same directory."
		);
	}

	/**
	 * Process command line arguments
	 *
	 * @param args
	 */
	public void process(String[] args) {
		Configuration configuration;

		try {
			configuration = argumentParser.parse(args);
		} catch (ArgumentException e) {
			System.out.println("Could not parse configuration:");
			System.out.println(e.getLocalizedMessage());
			showHelp();
			return;
		}

		configuration.setWarningReporter(new WarningReporter(ReportType.WARN));

		Compiler compiler = new CompilerWithStdlib(configuration);

		try {
			compiler.runCompilation();
		} catch (CompileException e) {
			System.out.println("Error while compiling:");
			System.out.println(e.getHelpfulMessage());
		} catch (InvalidInputException | IOException e) {
			System.out.println(e.getLocalizedMessage());
		}
	}
}
