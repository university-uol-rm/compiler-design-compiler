package cli.args;

import compiler.Configuration;
import output.DirectoryOutput;

import java.io.File;

public class ArgumentParser {
	public Configuration parse(String[] arguments) throws ArgumentException {
		Configuration configuration = new Configuration();

		if (arguments.length >= 1)
			configuration.setSourceDirectory(arguments[0]);
		else
			throw new ArgumentException("Must specify source directory");

		File target = configuration.getSourceDirectory();
		if (arguments.length >= 2)
			target = new File(arguments[1]);
		configuration.setOutput(new DirectoryOutput(target));

		return configuration;
	}
}
