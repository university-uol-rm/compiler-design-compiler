package cli.args;

/**
 * Represents an exception where there was an invalid argument passed
 */
public class ArgumentException extends Exception {
	public ArgumentException(String message) {
		super(message);
	}
}
