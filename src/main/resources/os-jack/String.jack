/**
 * Represents a String object. Implements the String type.
 */
class String {
  /**
   * Constructs a new empty String with a maximum length.
   */
  constructor String new(int maxLength) {}

  /**
   * De-allocates the string and frees its memory space.
   */
  method void dispose() {}

  /**
   * Returns the length of this String.
   */
  method int length() {}

  /**
   * Returns the character value at the specified index
   */
  method char charAt(int j) {}

  /**
   * Sets the j'th character of this string to the given character.
   */
  method void setCharAt(int j, char c) {}

  /**
   * Appends the given character to the end of this String, and returns the String.
   */
  method String appendChar(char c) {}

  /**
   * Erases the last character from this String.
   */
  method void eraseLastChar() {}

  /**
   * Returns the integer value of this String until the first non-numeric character.
   */
  method int intValue() {}

  /**
   * Sets this String to the representation of the given number.
   */
  method void setInt(int number) {}

  /**
   * Returns the new line character.
   */
  function char newLine() {}

  /**
   * Returns the backspace character.
   */
  function char backSpace() {}

  /**
   * Returns the double quote (") character.
   */
  function char doubleQuote() {}
}