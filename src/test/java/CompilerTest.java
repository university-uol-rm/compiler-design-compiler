import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import semantic.ReportType;
import semantic.SemanticException;
import semantic.WarningReporter;

import static org.junit.jupiter.api.Assertions.*;
import static util.Sample.getCompilerForPath;
import static util.Sample.getResourcePath;

public class CompilerTest {
	@ParameterizedTest
	@ValueSource(strings = {
			"Set 1/Fraction",
			"Set 1/HelloWorld",
			"Set 2/ArrayTest",
			"Set 3/Seven",
			"Set 4/MemoryTest",
			"Set 4/OutputTest",
			"Set 4/ScreenTest",
			"Set 4/StringTest"
	})
	void samplesCompileWithoutErrors(String pathname) {
		WarningReporter report = new WarningReporter(ReportType.INTERRUPT);

		assertDoesNotThrow(() -> {
			String path = getResourcePath(String.format("/sample/%s", pathname));
			getCompilerForPath(path, report).runCompilation();
		});

		assertEquals(0, report.getWarnings().size());
	}

	@ParameterizedTest
	@ValueSource(strings = {
			"Set 1/List",
			"Set 1/Average",
			"Set 1/Square",
			"Set 3/Average",
			"Set 3/Square",
			"Set 3/Pong",
			"Set 3/ConvertToBin",
			"Set 3/ComplexArrays",
			"Set 4/ArrayTest",
			"Set 4/MathTest",
			"Set 4/KeyboardTest",
			"Set 4/SysTest",
	})
	void samplesCompileWithSemanticWarnings(String pathname) {
		WarningReporter warn = new WarningReporter(ReportType.WARN);

		assertDoesNotThrow(() -> {
			String path = getResourcePath(String.format("/sample/%s", pathname));
			getCompilerForPath(path, warn).runCompilation();
		});

		assertNotEquals(0, warn.getWarnings().size());
	}

	@ParameterizedTest
	@ValueSource(strings = {
			"Set 2/ExpressionLessSquare",
			"Set 2/Square"
	})
	void samplesThrowSemanticErrors(String pathname) {
		WarningReporter warn = new WarningReporter(ReportType.WARN);

		assertThrows(SemanticException.class, () -> {
			String path = getResourcePath(String.format("/sample/%s", pathname));
			getCompilerForPath(path, warn).runCompilation();
		});

		assertNotEquals(0, warn.getWarnings().size());
	}
}
