package util;

import compiler.Compiler;
import compiler.CompilerWithStdlib;
import compiler.Configuration;
import lexical.Lexer;
import output.NullOutput;
import semantic.WarningReporter;
import syntactic.Parser;

import java.io.StringReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

public class Sample {
	public static Lexer sampleLexer(String source) {
		return new Lexer(new StringReader(source), null);
	}

	public static Parser sampleParser(String source) {
		Lexer lexer = new Lexer(new StringReader(source), null);
		return new Parser(lexer);
	}

	public static Compiler getCompilerForPath(String path, WarningReporter reporter) {

		Configuration configuration = new Configuration();
		configuration.setOutput(new NullOutput());
		configuration.setWarningReporter(reporter);
		configuration.setSourceDirectory(path);

		return new CompilerWithStdlib(configuration);
	}

	public static String getResourcePath(String resourcePath) {
		URL resource = Sample.class.getResource(resourcePath);
		try {
			return Paths.get(resource.toURI()).toString();
		} catch (URISyntaxException e) {
			throw new RuntimeException("Unexpected syntax exception", e);
		}
	}
}
