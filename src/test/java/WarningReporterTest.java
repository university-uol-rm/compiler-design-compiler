import lexical.data.Token;
import lexical.data.TokenType;
import org.junit.jupiter.api.Test;
import semantic.ReportType;
import semantic.SemanticException;
import semantic.WarningReporter;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class WarningReporterTest {
	@Test
	void testInterruptsCompilation() {
		WarningReporter reporter = new WarningReporter(ReportType.INTERRUPT);

		assertThrows(SemanticException.class, () -> {
			reporter.handle(createTestException());
		});
	}

	@Test
	void testWarnsOfErrors() {
		WarningReporter reporter = new WarningReporter(ReportType.WARN);

		assertEquals(0, reporter.getWarnings().size());

		SemanticException testException = createTestException();

		assertDoesNotThrow(() -> {
			reporter.handle(testException);
		});

		List<SemanticException> warnings = reporter.getWarnings();
		assertEquals(1, warnings.size());
		assertSame(testException, warnings.get(0));
	}

	private SemanticException createTestException() {
		return new SemanticException("Test", new Token(TokenType.SYMBOL, "!", 0, 0, ""));
	}
}