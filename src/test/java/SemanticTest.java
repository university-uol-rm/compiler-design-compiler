import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import semantic.ReportType;
import semantic.SemanticException;
import semantic.WarningReporter;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static util.Sample.getCompilerForPath;

class SemanticTest {
	private static Stream<File> getFailCases() {
		List<File> cases = new ArrayList<>();

		URL resource = SemanticTest.class.getResource("semanticExceptions");
		try {
			File containingDirectory = Paths.get(resource.toURI()).toFile();
			for (File testSet : Objects.requireNonNull(containingDirectory.listFiles())) {
				cases.addAll(Arrays.asList(
						Objects.requireNonNull(testSet.listFiles())
				));
			}
		} catch (URISyntaxException e) {
			throw new RuntimeException("Unexpected URISyntaxException", e);
		}

		return cases.stream();
	}

	@ParameterizedTest
	@MethodSource("getFailCases")
	void testSemanticErrorDetection(File program) {
		WarningReporter report = new WarningReporter(ReportType.INTERRUPT);

		assertThrows(SemanticException.class, () -> {
			getCompilerForPath(program.getPath(), report).runCompilation();
		});

		assertEquals(0, report.getWarnings().size());
	}
}