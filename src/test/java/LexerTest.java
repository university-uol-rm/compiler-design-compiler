import exception.LexicalException;
import exception.NoMoreTokensException;
import lexical.Lexer;
import lexical.data.Token;
import lexical.data.TokenType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static util.Sample.sampleLexer;

class LexerTest {
	@Test
	void emptySource() throws IOException, LexicalException {
		Lexer empty = sampleLexer("");
		assertTrue(empty.hasNextToken());
		assertEquals(
				new Token(TokenType.EOF, ""),
				empty.getNextToken()
		);
		assertFalse(empty.hasNextToken());
		assertThrows(NoMoreTokensException.class, empty::getNextToken);
	}

	@Test
	void simpleTokens() throws IOException, LexicalException {
		Lexer source = sampleLexer(" +     *");

		assertTrue(source.hasNextToken());
		assertEquals(
				new Token(TokenType.SYMBOL, "+"),
				source.getNextToken()
		);

		assertTrue(source.hasNextToken());
		assertEquals(
				new Token(TokenType.SYMBOL, "*"),
				source.getNextToken()
		);

		assertEquals(
				new Token(TokenType.EOF, ""),
				source.getNextToken()
		);

		assertFalse(source.hasNextToken());
		assertThrows(NoMoreTokensException.class, source::getNextToken);
	}

	@Test
	void peekToken() throws IOException, LexicalException {
		Lexer test = sampleLexer("2+3");

		for (int i = 0; i < 3; i++) {
			assertSame(test.peekNextToken(), test.peekNextToken());
			assertSame(test.peekNextToken(), test.getNextToken());
		}
	}

	@Test
	void singleLineComments() throws IOException, LexicalException {
		Lexer content = sampleLexer("+//plus\n-//minus");
		assertEquals(
				new Token(TokenType.SYMBOL, "+"),
				content.getNextToken()
		);
		assertEquals(
				new Token(TokenType.SYMBOL, "-"),
				content.getNextToken()
		);
		assertEquals(
				new Token(TokenType.EOF, ""),
				content.getNextToken()
		);
		assertFalse(content.hasNextToken());
	}

	@Test
	void multiLineComments() throws IOException, LexicalException {
		Lexer source = sampleLexer(
				"  +  /* here goes\n" +
						" a multi-line / block comment.\n" +
						"it doesn't end * until here: ***/* "
		);

		assertTrue(source.hasNextToken());
		assertEquals(
				new Token(TokenType.SYMBOL, "+"),
				source.getNextToken()
		);

		assertTrue(source.hasNextToken());
		assertEquals(
				new Token(TokenType.SYMBOL, "*"),
				source.getNextToken()
		);

		assertEquals(
				new Token(TokenType.EOF, ""),
				source.getNextToken()
		);
		assertFalse(source.hasNextToken());
		assertThrows(NoMoreTokensException.class, source::getNextToken);
	}

	@ParameterizedTest
	@ValueSource(strings = {
			"{a=(1+2);}",
			"{ a = ( 1 + 2 )   ;   }  ",
			"{\na=(1+2/* adding numbers, yea */); // hehehe \n}"
	})
	void symbols(String source) throws IOException, LexicalException {
		Lexer test = sampleLexer(source);

		assertEquals(
				new Token(TokenType.SYMBOL, "{"),
				test.getNextToken()
		);
		test.getNextToken();
		assertEquals(
				new Token(TokenType.SYMBOL, "="),
				test.getNextToken()
		);
		assertEquals(
				new Token(TokenType.SYMBOL, "("),
				test.getNextToken()
		);
		test.getNextToken();
		assertEquals(
				new Token(TokenType.SYMBOL, "+"),
				test.getNextToken()
		);
		test.getNextToken();
		assertEquals(
				new Token(TokenType.SYMBOL, ")"),
				test.getNextToken()
		);
		assertEquals(
				new Token(TokenType.SYMBOL, ";"),
				test.getNextToken()
		);
		assertEquals(
				new Token(TokenType.SYMBOL, "}"),
				test.getNextToken()
		);
	}

	@ParameterizedTest
	@ValueSource(strings = {
			"class Human { method laugh;boolean _arthur42=field}",
			"\n\nclass   Human{method laugh(boolean _arthur42)//hi\nfield}",
	})
	void keywordsAndIdentifiers(String source) throws IOException, LexicalException {
		Lexer test = sampleLexer(source);

		assertEquals(
				new Token(TokenType.KEYWORD, "class"),
				test.getNextToken()
		);
		assertEquals(
				new Token(TokenType.IDENTIFIER, "Human"),
				test.getNextToken()
		);
		test.getNextToken();
		assertEquals(
				new Token(TokenType.KEYWORD, "method"),
				test.getNextToken()
		);
		assertEquals(
				new Token(TokenType.IDENTIFIER, "laugh"),
				test.getNextToken()
		);
		test.getNextToken();
		assertEquals(
				new Token(TokenType.KEYWORD, "boolean"),
				test.getNextToken()
		);
		assertEquals(
				new Token(TokenType.IDENTIFIER, "_arthur42"),
				test.getNextToken()
		);
		test.getNextToken();
		assertEquals(
				new Token(TokenType.KEYWORD, "field"),
				test.getNextToken()
		);
	}

	@ParameterizedTest
	@ValueSource(strings = {
			"true-false=42 null*1984",
			"  true  - false = 42\n\nnull\n*1984\n",
	})
	void literals(String source) throws IOException, LexicalException {
		Lexer test = sampleLexer(source);

		assertEquals(
				new Token(TokenType.LITERAL_BOOLEAN, "true"),
				test.getNextToken()
		);
		test.getNextToken();
		assertEquals(
				new Token(TokenType.LITERAL_BOOLEAN, "false"),
				test.getNextToken()
		);
		test.getNextToken();
		assertEquals(
				new Token(TokenType.LITERAL_INTEGER, "42"),
				test.getNextToken()
		);
		assertEquals(
				new Token(TokenType.LITERAL_NULL, "null"),
				test.getNextToken()
		);
		test.getNextToken();
		assertEquals(
				new Token(TokenType.LITERAL_INTEGER, "1984"),
				test.getNextToken()
		);
	}

	@ParameterizedTest
	@ValueSource(strings = {
			"\"thhgttg\";\"\"",
			"  \"thhgttg\" ; \"\" ",
	})
	void strings(String source) throws IOException, LexicalException {
		Lexer test = sampleLexer(source);

		assertEquals(
				new Token(TokenType.LITERAL_STRING, "\"thhgttg\""),
				test.getNextToken()
		);
		test.getNextToken();
		assertEquals(
				new Token(TokenType.LITERAL_STRING, "\"\""),
				test.getNextToken()
		);
	}

	@ParameterizedTest
	@ValueSource(strings = {
			"\"",
			"\"hello w..\n\"",
			"3d",
			"3#4",
			"/**\n\n",
	})
	void exceptions(String source) {
		Lexer test = sampleLexer(source);
		assertThrows(LexicalException.class, () -> {
			while (test.hasNextToken()) test.getNextToken();
		});
	}

	@Test
	void exceptionLocations() throws IOException {
		Lexer test = sampleLexer("42s");
		try {
			test.getNextToken();
		} catch (LexicalException e) {
			assertEquals(1, e.getLine());
			assertEquals(2, e.getColumn());
		}

		Lexer test2 = sampleLexer("  \n   #");
		try {
			test2.getNextToken();
		} catch (LexicalException e) {
			assertEquals(2, e.getLine());
			assertEquals(3, e.getColumn());
		}
	}

	@Test
	void locations() throws IOException, LexicalException {
		Lexer test = sampleLexer(
				"*\n" +
						" function\n" +
						"  (\n"
		);

		Token star = test.getNextToken();
		assertEquals(1, star.getLine());
		assertEquals(0, star.getColumn());

		Token function = test.getNextToken();
		assertEquals(2, function.getLine());
		assertEquals(1, function.getColumn());

		Token parenthesis = test.getNextToken();
		assertEquals(3, parenthesis.getLine());
		assertEquals(2, parenthesis.getColumn());

		Token eof = test.getNextToken();
		assertEquals(4, eof.getLine());
		assertEquals(0, eof.getColumn());
	}
}