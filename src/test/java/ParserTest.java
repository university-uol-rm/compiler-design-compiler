import exception.CompileException;
import org.junit.jupiter.api.Test;
import syntactic.Parser;
import syntactic.data.symbol.Variable;
import syntactic.data.JackClass;
import syntactic.data.Program;
import syntactic.data.symbol.Subroutine;

import java.io.IOException;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static util.Sample.sampleParser;

public class ParserTest {
	@Test
	void classTree() throws IOException, CompileException {
		Parser test = sampleParser(
				"class Test  {\n" +
						"  field int x, y;" +
						"  function void f(int a)  {}" +
						"}"
		);

		Program program = new Program();
		test.parseProgram(program);
		Collection<JackClass> classes = program.getClasses();
		assertEquals(1, classes.size());

		JackClass jackClass = (JackClass) classes.toArray()[0];

		Collection<Variable> fields = jackClass.getFields();
		assertEquals(2, fields.size());

		Collection<Subroutine> subroutines = jackClass.getSubroutines();
		assertEquals(1, subroutines.size());
	}
}
